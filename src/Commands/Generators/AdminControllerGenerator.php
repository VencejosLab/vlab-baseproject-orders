<?php
namespace VLab\BaseOrders\Commands\Generators;

use CodeIgniter\CLI\BaseCommand;
use CodeIgniter\CLI\GeneratorTrait;

class AdminControllerGenerator extends BaseCommand
{
    use GeneratorTrait;

    /**
     * The Command's Group
     *
     * @var string
     */
    protected $group = 'Generators';

    /**
     * The Command's Name
     *
     * @var string
     */
    protected $name = 'make:controlleradmin';

    /**
     * The Command's Description
     *
     * @var string
     */
    protected $description = 'Generates a new controller file inside Admin area';

    /**
     * The Command's Usage
     *
     * @var string
     */
    protected $usage = 'make:controlleradmin <name> [options]';

    /**
     * The Command's Arguments
     *
     * @var array
     */
    protected $arguments = [
        'name' => 'The module name.',
    ];

    /**
     * The Command's Options
     *
     * @var array
     */
    protected $options = [
        '--force'     => 'Force overwrite existing file.'
    ];

    /**
     * Actually execute a command.
     */
    public function run(array $params)
    {
        $this->component = 'Controller';

        $this->directory =   'Controllers\Admin';
        $this->template  = 'controlleradmin.tpl.php';

        $this->classNameLang = 'CLI.generator.className.controller';
        $this->execute($params);
    }

    /**
     * Prepare options and do the necessary replacements.
     */
    protected function prepare(string $class): string
    {
        $useStatement = trim(APP_NAMESPACE, '\\') . '\Controllers\Admin\AdminBaseController';
        $extends      = 'AdminBaseController';
        $viewsFolder = ucfirst($this->params['name']);

        return $this->parseTemplate(
            $class,
            ['{useStatement}', '{extends}', '{module}', '{viewsFolder}'],
            [$useStatement, $extends, $this->params['name'], $viewsFolder],
            ['type' => 'presenter']
        );
    }
}
