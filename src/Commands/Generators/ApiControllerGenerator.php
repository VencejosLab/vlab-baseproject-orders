<?php
namespace VLab\BaseOrders\Commands\Generators;

use CodeIgniter\CLI\BaseCommand;
use CodeIgniter\CLI\GeneratorTrait;

class ApiControllerGenerator extends BaseCommand
{
    use GeneratorTrait;

    /**
     * The Command's Group
     *
     * @var string
     */
    protected $group = 'Generators';

    /**
     * The Command's Name
     *
     * @var string
     */
    protected $name = 'make:apicontroller';

    /**
     * The Command's Description
     *
     * @var string
     */
    protected $description = 'Generates a new api controller file';

    /**
     * The Command's Usage
     *
     * @var string
     */
    protected $usage = 'make:apicontroller <name> [options]';

    /**
     * The Command's Arguments
     *
     * @var array
     */
    protected $arguments = [
        'name'      => 'The controller name.',
        'model'     => 'The model class name',
        'entity'    => 'The entity class name'
    ];

    /**
     * The Command's Options
     *
     * @var array
     */
    protected $options = [
        '--force'     => 'Force overwrite existing file.'
    ];

    /**
     * Actually execute a command.
     */
    public function run(array $params)
    {
        $this->component = 'Controller';

        $this->directory =   'api';
        $this->template  = 'apicontroller.tpl.php';

        //this->classNameLang = 'CLI.generator.className.controller';
        $this->execute($params);
    }

    /**
     * Prepare options and do the necessary replacements.
     */
    protected function prepare(string $class): string
    {
        // $useStatement = trim(APP_NAMESPACE, '\\') . '\Api';
        $extends      = 'ResourceController';


        return $this->parseTemplate(
            $class,
            ['{extends}', '{entityClass}', '{modelClass}'],
            [$extends, $this->params['entity'], $this->params['model'] ]
        );
    }
}
