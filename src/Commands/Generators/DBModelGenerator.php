<?php
namespace VLab\BaseOrders\Commands\Generators;

use CodeIgniter\CLI\BaseCommand;
use CodeIgniter\CLI\CLI;
use CodeIgniter\CLI\GeneratorTrait;

class DBModelGenerator extends BaseCommand
{
    use GeneratorTrait;

    /**
     * The Command's Group
     *
     * @var string
     */
    protected $group = 'Generators';

    /**
     * The Command's Name
     *
     * @var string
     */
    protected $name = 'make:dbmodel';

    /**
     * The Command's Description
     *
     * @var string
     */
    protected $description = 'Generates a new model file with fields from db table.';

    /**
     * The Command's Usage
     *
     * @var string
     */
    protected $usage = 'make:model <name> [options]';

    /**
     * The Command's Arguments
     *
     * @var array
     */
    protected $arguments = [
        'name' => 'The model class name.',
    ];

    /**
     * The Command's Options
     *
     * @var array
     */
    protected $options = [
        '--table'     => 'Supply a table name. Default: "the lowercased plural of the class name".',
        '--dbgroup'   => 'Database group to use. Default: "default".',
        '--return'    => 'Return type, Options: [array, object, entity]. Default: "array".',
        '--namespace' => 'Set root namespace. Default: "APP_NAMESPACE".',
        '--suffix'    => 'Append the component title to the class name (e.g. User => UserModel).',
        '--force'     => 'Force overwrite existing file.',
    ];

    /**
     * Actually execute a command.
     */
    public function run(array $params)
    {
        helper('inflector');
        $this->component = 'Model';
        $this->directory = 'Models';
        $this->template  = 'dbmodel.tpl.php';

        $this->classNameLang = 'CLI.generator.className.model';
        $this->execute($params);
    }

    /**
     * Prepare options and do the necessary replacements.
     */
    protected function prepare(string $class): string
    {
        $table   = $this->getOption('table');
        $dbGroup = $this->getOption('dbgroup');
        $return  = $this->getOption('return');

        $baseClass = class_basename($class);
        $baseClassEntity = class_basename($class);

         if (preg_match('/^(\S+)Model$/i', $baseClass, $match) === 1) {
            $baseClass = $match[1];
            $baseClassEntity = singular($match[1]);
        }

        $table   = is_string($table) ? $table : plural(strtolower($baseClass));
        $dbGroup = is_string($dbGroup) ? $dbGroup : 'default';
        $return  = is_string($return) ? $return : 'array';

        if (!in_array($return, ['array', 'object', 'entity'], true)) {
            // @codeCoverageIgnoreStart
            $return = CLI::prompt(lang('CLI.generator.returnType'), ['array', 'object', 'entity'], 'required');
            CLI::newLine();
            // @codeCoverageIgnoreEnd
        }

        if ($return === 'entity') {
            $return = str_replace('Models', 'Entities', $class);

            if (preg_match('/^(\S+)Model$/i', $return, $match) === 1) {
                // $return = $match[1];
                $return = "App\\Entities\\" . $baseClassEntity;
                if ($this->getOption('suffix')) {
                    $return .= 'Entity';
                }
            }

            $return = '\\' . trim($return, '\\') . '::class';
            $this->call('make:entity', array_merge([$baseClassEntity], $this->params));
        } else {
            $return = "'{$return}'";
        }
        $allowedFields = $this->getAllowedFields();

        return $this->parseTemplate($class, ['{table}', '{dbGroup}', '{return}', '{allowedFields}'], [$table, $dbGroup, $return, $allowedFields]);
    }

    private function getAllowedFields(){
        if(null == $tableFields = $this->getTableFields()){
            return null; 
        }
        return implode(', ', array_map(function($val){return sprintf("'%s'", $val);}, $tableFields));
    }

    private function getTableFields()
    {
        try {
            if ($this->params['table']) {
                $db = db_connect();

                if (!$db->tableExists($this->params['table'])) {
                    CLI::write("La table {$this->params['table']} no existe en la base de datos.", 'yellow');
                    return null;
                }

                $excludeFields = ['created_at', 'updated_at', 'deleted_at'];

                $fields = $db->getFieldNames($this->params['table']);

                return array_diff($fields, $excludeFields);

            } else {
                return null;
            }
        } catch (\Throwable $th) {
            log_message('error', $th->getMessage());
            return null;
        }
    }
}
