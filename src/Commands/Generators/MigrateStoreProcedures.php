<?php

namespace VLab\BaseOrders\Commands\Generators;

use CodeIgniter\CLI\BaseCommand;
use CodeIgniter\CLI\CLI;
use Config\Database;

class MigrateStoreProcedures extends BaseCommand
{
    protected $group       = 'Database';
    protected $name        = 'migrate:sp';
    protected $description = 'Runs stored procedures from a directory in the current database connection.';
    protected $db;
    const DELIMITER = "$$";
    const DIRECTORY = FCPATH . '../Scripts/SP/';

    public function run(array $params)
    {
        $result = [];
        //$params empty or only '*' element
        if (sizeof($params) == 0 || (sizeof($params) == 1 && $params[0] == '*')) {
            $files = preg_grep('~\.(sql)$~', scandir(self::DIRECTORY));
            $totalSteps = count($files);
            $currStep   = 1;
            foreach ($files as $key => $file) {
                try {
                    $result[$file] = $this->run_sql_file($file);
                    CLI::showProgress($currStep++, $totalSteps);
                } catch (\Throwable $th) {
                    continue;
                }
            }
            CLI::showProgress(false);
        } else {
            $totalSteps = count($params);
            $currStep   = 1;
            //$params with files's name to run
            foreach ($params as $key => $par) {
                try {
                    $file = trim($par) . '.sql';
                    $exists = file_exists(self::DIRECTORY . $file);
                    if (!$exists) {
                        $result[] = ['name' => $file, 'total' => 0, 'success' => 0, 'error' => "Archivo no encontrado"];
                        CLI::showProgress($currStep++, $totalSteps);
                        continue;
                    }
                    $result[$file] = $this->run_sql_file($file);
                    CLI::showProgress($currStep++, $totalSteps);
                } catch (\Throwable $th) {
                    continue;
                }
            }
            CLI::showProgress(false);
        }

        $this->showResult($result);
    }

    public function run_sql_file($filename)
    {
        $this->db = \Config\Database::connect();
        try {
            //load file
            $content = file_get_contents(self::DIRECTORY . $filename);

            //exclude comments and DELIMITER command
            $lines = explode("\n", $content);
            $commands = '';
            foreach ($lines as $line) {
                $line = trim($line);
                if ($line && !$this->startsWith($line, '#') && !$this->startsWith($line, "DELIMITER")) {
                    $commands .= $line . "\n";
                }
            }

            //convert to array
            $commands = explode(self::DELIMITER, $commands);

            //run commands
            $total = $success = 0;
            foreach ($commands as $command) {
                if (trim($command)) {
                    $command = trim($command);
                    $success += ($this->db->query($command) == false ? 0 : 1);
                    $total += 1;
                }
            }

            //return number of successful queries and total number of queries found
            return array(
                "success" => $success,
                "total" => $total
            );
        } catch (\Throwable $th) {
            return array(
                "success" => 0,
                "total" => 0,
                "error" => $th->getMessage()
            );
        } finally {
            $this->db->close();
        }
    }

    // Here's a startsWith function
    function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    public function showResult(array $result)
    {
        $i = 0;
        foreach ($result as $key => $value) {
            if (array_key_exists('error', $value)) {
                CLI::write("{$i} {$key}: {$value['error']}", 'red');
                continue;
            }
            CLI::write("{$i} Store procedure: {$key} | Lines: {$value['total']} | Success: {$value['success']}", $value['total'] == $value['success'] ? 'green' : 'yellow');

            $i++;
        }
    }
}
