<?php
namespace VLab\BaseOrders\Commands\Generators;

use CodeIgniter\CLI\BaseCommand;
use CodeIgniter\CLI\GeneratorTrait;
use CodeIgniter\CLI\CLI;

class ScaffoldModuleAdminGenerator extends BaseCommand
{
    use GeneratorTrait;

    /**
     * The Command's Group
     *
     * @var string
     */
    protected $group = 'Generators';

    /**
     * The Command's Name
     *
     * @var string
     */
    protected $name = 'make:scaffoldmoduleadmin';

    /**
     * The Command's Description
     *
     * @var string
     */
    protected $description = 'Generates a new controller file inside Admin area';

    /**
     * The Command's Usage
     *
     * @var string
     */
    protected $usage = 'make:scaffoldmoduleadmin <name> [options]';

    /**
     * The Command's Arguments
     *
     * @var array
     */
    protected $arguments = [
        'name' => 'The module name.',
    ];

    /**
     * The Command's Options
     *
     * @var array
     */
    protected $options = [
        '--force'       => 'Force overwrite existing file.',
        '--tabledb'     => 'Database table name'
    ];

    /**
     * Actually execute a command.
     */
    public function run(array $params)
    {
        helper('inflector');

        $this->params = $params;

        $this->call('make:controlleradmin', ['name' => $params[0], 'force' => true, 'suffix' => true]);


        $this->call('make:view', [
            'name' => "Index",
            'area' => "Admin",
            'module' => $params[0],
            'tpl' => "view_admin_index"
        ]);
        $this->call('make:view', [
            'name' => "Form",
            'area' => "Admin",
            'module' => strtolower($params[0]),
            'tpl' => "view_admin_form"
        ]);
        $this->call('make:view', [
            'name' => "Datatable",
            'area' => "Admin",
            'module' => strtolower($params[0]),
            'tpl' => "view_admin_datatable"
        ]);

        //make:dbmodel Nuevo --table shippings --force true --return entity --suffix true
        $this->call('make:dbmodel', ['name' => $params[0], 'suffix' => true, 'force' => true, 'table' => $params['tabledb'] ?? "", 'return' => 'entity']);

        $modelClass = ucfirst($params[0]) . "Model";
        $entityClass = ucfirst(singular($params[0])) . 'Entity';

        $this->call('make:apicontroller', ['name' => $params[0], 'force' => true, 'model' => $modelClass, 'entity' => $entityClass]);

        $this->generateJSFolder('admin', $params[0]);
    }

    private function generateJSFolder($area, $module)
    {
        try {
            $resultFolder = true;
            $module = strtolower($module);
            $path = FCPATH . "src\\js\\{$area}\\{$module}";
            if (!is_dir($path)) {
                $resultFolder = mkdir($path, 0777, TRUE);
            }

            if ($resultFolder) {
                if (!write_file("{$path}\\datatable.js", "")) {
                    CLI::write("Error creating js datatable.js", 'red');
                } else {
                    CLI::write("File {$path}/datatable.js created successfully", 'green');
                }

                if (!write_file("{$path}\\form.js", "")) {
                    CLI::write("Error creating js form.js", 'red');
                } else {
                    CLI::write("File {$path}/form.js created successfully", 'green');
                }
            }
        } catch (\Throwable $th) {
            $this->showError($th);
            return false;
        }
    }
}
