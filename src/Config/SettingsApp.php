<?php

namespace Config;

use CodeIgniter\Config\BaseConfig;
use VLab\BaseOrders\Models\SettingsModel;
use Exception;
use InvalidArgumentException;
use PhpParser\Node\Expr\Throw_;

class SettingsApp extends BaseConfig
{
    private $items = [];

    public function __construct()
    {
        parent::__construct();
        $model = new SettingsModel();


        if (!$settings = cache('settingsApp')) {
            $settings = $model->where('company_id', session('company_id'))->findAll();
            // Save into the cache for 15 minutes
            cache()->save('settingsApp', $settings, 900);
        }

        $this->items = $settings;
    }

    public function get($settingName)
    {
        try {
            if ($settingName == "" || $settingName == NULL) throw new InvalidArgumentException("El parametro settingName es requerido");

            $settingName = trim($settingName);
            $result = array_filter($this->items, fn ($el) => $el['key'] == $settingName);

            if (sizeof($result) == 0) throw new  \Exception("Setting con el nombre {$settingName} no existe.");

            return array_values($result)[0];
        } catch (\Throwable $th) {
            log_message('error', $th->__toString());
            throw $th;
        }
    }
}
