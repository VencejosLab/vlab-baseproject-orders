<?php

namespace VLab\BaseOrders\Controllers\Admin;

use VLab\BaseOrders\Controllers\Admin\AdminBaseController;
use CodeIgniter\HTTP\RequestInterface;
use Psr\Log\LoggerInterface;
use CodeIgniter\HTTP\ResponseInterface;

class HomeController extends AdminBaseController
{
    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);
    }

    public function index()
    {
        $current_module = '';
        $data['permissions'] = permissions($current_module, $this->permissions);
        return view('Admin/Home/Index', $data);
    }
}
