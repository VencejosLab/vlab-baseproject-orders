<?php

namespace VLab\BaseOrders\Controllers;

use VLab\BaseOrders\Controllers\BaseController;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
use CodeIgniter\HTTP\RedirectResponse;

class Auth extends BaseController
{
    protected $validation;
    protected $auth;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);
        $this->auth         = service('auth');
        $this->validation = \Config\Services::validation();
    }

    public function forceResetPassword(){
        if(!auth()->loggedIn()){
            return redirect('login');
        }
        return view('Auth/ResetPassword');
    }

    public function forceResetPasswordAction(){
        try {
            $rules = [
                'password' => [
                    'label' => 'Auth.password',
                    'rules' => [
                        'required',
                        'max_byte[72]',
                    ],
                    'errors' => [
                        'max_byte' => 'Auth.errorPasswordTooLongBytes',
                    ]
                ],
                'password_confirm' => [
                    'label' => 'Auth.passwordConfirm',
                    'rules' => 'required|matches[password]',
                ]
            ];

            $this->validation->setRules($rules);

            if (! $this->validation->run($this->request->getPost())) {
                return redirect('login')->with('errors', $this->validation->getErrors());
            }

            $users = auth()->getProvider();
            $user = $users->findById($this->request->getPost('id'));

            if(!$user){
               return redirect('login')->with('error', lang('Auth.userDoesNotExist'));
            }

            $password = (string) $this->request->getPost('password');
            $user->setPassword($password);
            $users->save($user);
            
            $user->activate();
            $user->undoForcePasswordReset();

            $url = config('Auth')->logoutRedirect();
            auth()->logout();
            return redirect()->to($url)->with('message', lang(lang('Auth.passwordChangeSuccess')));
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return redirect('login')->with('error', lang('Errors.common'));
        }
    }
}
