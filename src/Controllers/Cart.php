<?php

namespace VLab\BaseOrders\Controllers;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
use VLab\BaseOrders\Models\CategoriesModel;

class Cart extends BaseController
{
    protected $validation;
    protected $auth;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);
        $this->auth         = service('auth');
        $this->validation = \Config\Services::validation();
    }

    public function index()
    {
        $categoryModel = new CategoriesModel();
        $data['categories_menu'] = $categoryModel->where('show_in_menu', 1)->orderBy('name', 'asc')->findAll();
        return view('Cart/Index', $data);
    }

    public function success()
    {
        $data['numero'] = $this->request->getGet('NP');
        $categoryModel = new CategoriesModel();
        $data['categories_menu'] = $categoryModel->where('show_in_menu', 1)->orderBy('name', 'asc')->findAll();
        return View("Cart/Success", $data);
    }
}
