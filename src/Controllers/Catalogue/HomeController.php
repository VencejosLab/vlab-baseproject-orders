<?php

namespace VLab\BaseOrders\Controllers\Catalogue;

use VLab\BaseOrders\Controllers\Catalogue\CatalogueBaseController;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
use VLab\BaseOrders\Models\CategoriesModel;

class HomeController extends CatalogueBaseController
{
    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);
    }

    public function index()
    {
        if (!checkUserHasRequiredData()) {
            session()->setFlashdata('message', "Debe completar su información antes de continuar.");
            return redirect('catalogue/profile');
        }
        $categoryModel = new CategoriesModel();
        $data['categories_menu'] = $categoryModel->where('show_in_menu', 1)->orderBy('name', 'asc')->findAll();
        return view('Catalogue/Home/Index', $data);
    }



    // public function category($name = null){
    //     try {
    //         if($name == "" || $name == NULL){
    //             return $this->response->redirect("/catalogue");
    //         }

    //         $name = strtolower(trim(rawurldecode($name)));

    //         $categoryModel = new CategoriesModel();

    //         $category = $categoryModel->where('LOWER(name)', $name)->first();

    //         if($category == NULL){
    //             return $this->response->redirect("/catalogue");
    //         }

    //         return view('Catalogue/Home/Index', []);
    //     } catch (\Throwable $th) {
    //         log_message('error', "Error: " . $th->__toString());
    //         throw $th;
    //     }
    // }

    public function myorders()
    {
        try {
            $modelClient = new ClientsModel();
            $userId = auth()->id();
            $categoryModel = new CategoriesModel();
            $data['categories_menu'] = $categoryModel->where('show_in_menu', 1)->orderBy('name', 'asc')->findAll();
            $data['clientId'] = $modelClient->where(['user_id' => $userId, 'company_id' => session('company_id')])->first()->id;
            return view('Catalogue/Home/MyOrders', $data);
        } catch (\Throwable $th) {
            log_message('error', "Error: " . $th->__toString());
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }
    }

    public function profile()
    {
        try {
            $categoryModel = new CategoriesModel();
            $data['categories_menu'] = $categoryModel->where('show_in_menu', 1)->orderBy('name', 'asc')->findAll();
            return view('Catalogue/Home/UserProfile', $data);
        } catch (\Throwable $th) {
            log_message('error', "Error: " . $th->__toString());
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }
    }
}
