<?php

namespace VLab\BaseOrders\Controllers;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

class Demo extends BaseController
{

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);
    }

    public function index()
    {
        return view('Demo/Home/Index');
    }

    public function checkout()
    {
        return view('Demo/Home/Checkout');
    }

    public function catalogue()
    {
        return view('Demo/Home/Catalogue');
    }
}
