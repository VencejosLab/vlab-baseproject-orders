<?php

namespace VLab\BaseOrders\Controllers;

use CodeIgniter\Config\BaseConfig;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Config\SettingsApp;
use Dompdf\Options;
use Psr\Log\LoggerInterface;
use Codeigniter\Exceptions\DownloadException;
use VLab\BaseOrders\Entities\Enums\OrderStatusEnum;
use VLab\BaseOrders\Models\OrdersModel;

class ReportController extends BaseController
{
    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);
    }

    /**
     * Download order pdf report from client panel
     */
    public function order($id = null, $number = null, $download = false, $currency = null)
    {
        try {
            if (is_null($id) || is_null($number)) {
                return redirect()->to('/');
            }

            if($currency == NULL){
                $config = new SettingsApp();
                $currency = $config->get('default_currency')['value'];
            }

            $modelPedido = new OrdersModel();
            $dataPedido['data'] =  $modelPedido->getWithDetails($id, $currency);

            if ($dataPedido['data']['number'] != $number) {
                throw new \CodeIgniter\Exceptions\PageNotFoundException();
            }

            $dompdf = new \Dompdf\Dompdf();
            $filename = env('NAME_COMPANY') . " - {$dataPedido['data']['number']}.pdf";
            $viewHtml = '';

            if ($dataPedido['data']['status'] == OrderStatusEnum::CONFIRMED) {
                $viewHtml = view('Reports/OrderConfirmed', $dataPedido);
            } else {
                $viewHtml = view('Reports/Order', $dataPedido);
            }

            $dompdf->loadHtml($viewHtml);

            $dompdf->setPaper('A4', 'portrait');
            $options = new Options([
                'defaultMediaType' => 'all',
                'isFontSubsettingEnabled' => true,
                'isRemoteEnabled'   => true,
                'isPhpEnabled' => true,
                'enableCssFloat' => true
            ]);
            $dompdf->setOptions($options);
            $dompdf->render();
            $dompdf->stream($filename, array('Attachment' => $download));
        } catch (\Throwable $th) {
            log_message('error', $th->__toString());
            throw $th;
        }
    }
}
