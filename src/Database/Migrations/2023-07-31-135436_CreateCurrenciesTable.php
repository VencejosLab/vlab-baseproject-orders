<?php

namespace VLab\BaseOrders\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateCurrenciesTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'unsigned' => true,
                'auto_increment' => true
            ],
            'currency' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null' => false
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null' => true
            ],
            'iso_code' => [
                'type' => 'VARCHAR',
                'constraint' => 10,
                'null'  => true
            ],
            'exchange_rate' => [
                'type'  => 'DECIMAL',
                'constraint' => '10,4',
                'null'  => true,
                'default' => 1
            ],
            'company_id' => [
                'type'  => 'INT',
                'null'  => false
            ],
            'active' => [
                'type' => 'TINYINT',
                'default' => 1
            ],
            'created_by' => [
                'type' => 'VARCHAR',
                'null' => false,
                'constraint' => 100
            ],
            'updated_by' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null'  => true
            ],
            'deleted_by' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null'  => true
            ],
            'created_at datetime default current_timestamp',
            'updated_at datetime default current_timestamp on update current_timestamp',
            'deleted_at datetime'
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('currencies');
    }

    public function down()
    {
        $this->forge->dropTable('currencies');
    }
}
