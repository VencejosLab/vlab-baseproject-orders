<?php

namespace VLab\BaseOrders\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateListsPriceTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'unsigned' => true,
                'auto_increment' => true
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => 250,
                'null' => false
            ],
            'description' => [
                'type' => 'VARCHAR',
                'constraint' => 250,
                'null' => false
            ],
            'margin' => [
                'type' => 'DECIMAL',
                'constraint' => '10,4',
                'null' => false
            ],
            'company_id' => [
                'type'  => 'INT',
                'null'  => false
            ],
            'created_by' => [
                'type' => 'VARCHAR',
                'null' => false,
                'constraint' => 100
            ],
            'updated_by' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null'  => true
            ],
            'deleted_by' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null'  => true
            ],
            'created_at datetime default current_timestamp',
            'updated_at datetime default current_timestamp on update current_timestamp',
            'deleted_at datetime'
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('lists_price');
    }

    public function down()
    {
        $this->forge->dropTable('lists_price');
    }
}
