<?php

namespace VLab\BaseOrders\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateBulkPricingTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'unsigned' => true,
                'auto_increment' => true
            ],
            'product_id' => [
                'type' => 'INT',
                'unsigned' => true
            ],
            'min_bulk' => [
                'type' => 'DECIMAL',
                'constraint' => '10,4',
                'null' => true
            ],
            'max_bulk' => [
                'type' => 'DECIMAL',
                'constraint' => '10,4',
                'null' => true
            ],
            'discount' => [
                'type' => 'DECIMAL',
                'constraint' => '10,4',
                'default' => 0
            ],
            'sale_price' => [
                'type' => 'DECIMAL',
                'constraint' => '10,4',
                'default' => 0
            ],
            'unit_sale_price' => [
                'type' => 'DECIMAL',
                'constraint' => '10,4',
                'default' => 0
            ],
            'valid_from datetime',
            'valid_until datetime',
            'active' => [
                'type' => 'TINYINT',
                'default' => 1
            ],
            'allow_select_qty' => [
                'type' => 'TINYINT',
                'default' => 1
            ],
            'company_id' => [
                'type'  => 'INT',
                'null'  => false
            ],
            'created_by' => [
                'type' => 'VARCHAR',
                'null' => false,
                'constraint' => 100
            ],
            'updated_by' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null'  => true
            ],
            'deleted_by' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null'  => true
            ],
            'created_at datetime default current_timestamp',
            'updated_at datetime default current_timestamp on update current_timestamp',
            'deleted_at datetime'
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->addKey('product_id');
        $this->forge->addForeignKey('product_id', 'products', 'id', '', 'CASCADE', 'fk_bulk_pricing_products');
        $this->forge->createTable('bulk_pricing');
    }

    public function down()
    {
        $this->forge->dropTable('bulk_pricing');
    }
}
