<?php

namespace VLab\BaseOrders\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddFieldsSettingsTable extends Migration
{
    public function up()
    {
        $addfields = [
            'group' => [
                'type' => 'VARCHAR',
                'constraint' => '20',
                'null'    => true
            ],
            'sub_group' => [
                'type' => 'VARCHAR',
                'constraint' => '20',
                'null'    => true
            ],
            'company_id' => [
                'type' => 'INT',
                'null'    => false
            ],
            'created_by' => [
                'type' => 'VARCHAR',
                'null' => false,
                'constraint' => 100
            ],
            'updated_by' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null'  => true
            ]
        ];
        $this->forge->addColumn('settings', $addfields);
    }

    public function down()
    {
        $this->forge->dropColumn('settings', ['group']);
        $this->forge->dropColumn('settings', ['sub_group']);
        $this->forge->dropColumn('settings', ['company_id']);
        $this->forge->dropColumn('settings', ['updated_by']);
        $this->forge->dropColumn('settings', ['created_by']);
    }
}
