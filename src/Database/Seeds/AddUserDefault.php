<?php

namespace VLab\BaseOrders\Database\Seeds;

use CodeIgniter\Database\Seeder;
use CodeIgniter\CLI\CLI;

class AddUserDefault extends Seeder
{
    /**
     * 1) Crea un registro en la tabla user para un usuario de prueba
     * 2) Crea un registro en la tabla auth_identities para el usuario creado en el paso 1
     * 3) Crea un registro en la tabla auth_groups_users para asignar el rol de user al usuario creado en el paso 1
     * 4) Crea un registro en la tabla clients para el enlazar con el user_id creado en el paso 1
     * 5) Asignar los permisos al usuario creado en el paso 1
     */

    public function run()
    {
        try {
            $this->db->transBegin();
            //1)
            $dataUser = [
                'username'      => 'user1',
                'active'        => 1,
                'company_id'    => 1,
                'created_at'    =>  date("Y-m-d H:i:a", time())
            ];

            $this->db->table('users')->insert($dataUser);

            $userId = $this->db->insertID();

            //2)
            $dataIdentity = [
                'user_id'       => $userId,
                'name'          => 'Comprador 1',
                'type'          => 'email_password',
                'secret'        => 'info@vencejoslab.com',
                'secret2'       => '$2y$10$HdQg2qGovYYLyT29kVc9oOAV/I85lYu8vw2fXSoLv9.RLnGKnNzIq', //qwe123RT$%
                'force_reset'   => 0,
                'created_at'    =>  date("Y-m-d H:i:a", time())
            ];

            $this->db->table('auth_identities')->insert($dataIdentity);

            //3)
            $dataUserGroup = [
                'user_id'   => $userId,
                'group'     => 'user',
                'created_at' =>  date("Y-m-d H:i:a", time())
            ];

            $this->db->table('auth_groups_users')->insert($dataUserGroup);

            //4)
            $dataPermissionsUser = [];
            $config = config(\Config\AuthGroups::class);

            $permissionsGroup = $config->matrix['user']; //permisos del grupo
            $permissionsConfig = array_keys($config->permissions); //todos los permisos definidos

            for ($i = 0; $i < sizeof($permissionsGroup); $i++) {
                $permissionExplode =  explode('.', $permissionsGroup[$i]);
                $module = $permissionExplode[0];
                $permission = $permissionExplode[1];

                if ($permission == "*") {
                    $permissionsModule = array_values(array_filter($permissionsConfig, function ($v, $k) use ($module) {
                        return explode('.', $v)[0] == $module;
                    }, ARRAY_FILTER_USE_BOTH));

                    for ($j = 0; $j < sizeof($permissionsModule); $j++) {
                        $dataPermissionsUser[] = [
                            'user_id'       => $userId,
                            'permission'    => $permissionsModule[$j],
                            'created_at'    => date("Y-m-d H:i:a", time())
                        ];
                    }
                } else {
                    $dataPermissionsUser[] = [
                        'user_id'       => $userId,
                        'permission'    => $permissionsGroup[$i],
                        'created_at'    => date("Y-m-d H:i:a", time())
                    ];
                }
            }

            if (sizeof($dataPermissionsUser) > 0)
                $this->db->table('auth_permissions_users')->insertBatch($dataPermissionsUser);

            //5)
            $dataClient = [
                'user_id'       => $userId,
                'company_id'    => 1,
                'business_name' => 'CLIENTE PRUEBA',
                'email'         => $dataIdentity['secret']
            ];

            $this->db->table('clients')->insert($dataClient);

            $this->db->transComplete();

            CLI::write("user_id created: {$userId}", 'green');
        } catch (\Throwable $th) {
            CLI::write($th->getMessage(), 'red');
        }
    }
}
