<?php

namespace VLab\BaseOrders\Entities;

use CodeIgniter\Entity\Entity;

class BrandEntity extends Entity
{
    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [];
    protected $attributes = ['id', 'name', 'company_id', 'created_by', 'updated_by', 'deleted_by'];
}
