<?php

namespace VLab\BaseOrders\Entities;

use CodeIgniter\Entity\Entity;

class CurrencyEntity extends Entity
{
    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [
        'active'    => 'bool'
    ];

    protected $attributes = [
        'id'            => 0, 
        'currency'      => null,
        'name'          => null,
        'iso_code'      => null,
        'exchange_rate' => null,
        'company_id'    => null,
        'active'        => true
    ];

    
    public function setActive($value)
    {
        $this->attributes['active'] = $value ? "1" : "0";
    }
}
