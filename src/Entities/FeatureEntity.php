<?php

namespace VLab\BaseOrders\Entities;

use CodeIgniter\Entity\Entity;

class FeatureEntity extends Entity
{
    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [];
    protected $attributes = [
        'id'            => 0,
        'name'          => null,
        'position'      => null,
        'company_id'    => null,
        'created_by'    => null,
        'updated_by'    => null,
        'deleted_by'    => null,
        'values'        => []
    ];
}
