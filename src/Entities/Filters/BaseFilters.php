<?php

namespace VLab\BaseOrders\Entities\Filters;

use CodeIgniter\Entity\Entity;

class BaseFilters extends Entity{

    /**
     * Indicador para mostrar o no los registros eliminados
     * @var bool
     */
    public $MostrarEliminados;

    /**
     * Nombre y orientación de la columna para ordenar los resultados
     * @var string
     */
    public $orden;

    /**
     * Página de inicio para el paginado
     * @var int
     */
    public $PageIndex;

    /**
     * Cantidad de registros a mostrar por página
     * @var int
     */
    public $PageSize;

    public function __construct(){
    }
}
?>