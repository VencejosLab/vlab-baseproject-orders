<?php

namespace VLab\BaseOrders\Entities\Filters;

use CodeIgniter\Entity\Entity;

class CategoriesFilters extends Entity
{
    protected $attributes = [
        'name'            => '',
        //Utilizados para el paginado y ordenación
        'PageIndex'         => 1,
        'PageSize'          => 10,
        'OrderBy'          => ''
    ];

}
