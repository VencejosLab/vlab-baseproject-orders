<?php

namespace VLab\BaseOrders\Entities\Filters;

use CodeIgniter\Entity\Entity;

class UsersFilters extends Entity
{
    protected $attributes = [
        'nombre'            => '',
        'username'          => '',
        'email'             => '',
        'soloActivos'       => 1,
        //Utilizados para el paginado y ordenación
        'PageIndex'         => 1,
        'PageSize'          => 10,
        'Ordenar'           => 'nombre ASC'
    ];

    public function MostrarActivos()
    {
        if ($this->attributes["soloActivos"] == "true")
            return 0;
        else
            return 1;
    }
}
