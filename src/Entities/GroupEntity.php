<?php

namespace VLab\BaseOrders\Entities;

use CodeIgniter\Entity\Entity;

class GroupEntity extends Entity
{
    protected $attributes = [
        'id'            => 0,
        'name'          => '',
        'description'   => ''
    ];
    
    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [];
}
