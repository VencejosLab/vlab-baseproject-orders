<?php

namespace VLab\BaseOrders\Entities;

use CodeIgniter\Entity\Entity;

class OrderEntity extends Entity
{
    protected $attributes = [
        'id'                    => 0,
        'number'                => "",
        'cliente_id'            => 0,
        'user_id'               => 0,
        'date'                  => null,
        'confirmation_date'     => null,
        'delivery_date'         => null,
        'shipping_date'         => null, 
        'shipping_status'       => "", 
        'free_shipping'         => "",
        'client_fullname'       => "",
        'estimate_id'           => null,
        'currency_id'           => null,
        'exchange_rate'         => 1,
        'discount_percentage'   => 0,
        'sale_condition_id'     => null,
        'credit_term_id'        => null,
        'notes'                 => null,
        'total'                 => 0,
        'total_discount'        => 0,
        'total_taxes'           => 0,
        'total_net'             => 0,
        'company_id'            => null,
        'created_by'            => '',
        'updated_by'            => '',
        'deleted_by'            => ''
    ];


    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [
        'free_shipping' => 'boolean'
    ];

    public function setFree_shipping($value)
    {
        $this->attributes['free_shipping'] = $value ? "1" : "0";
    }
}
