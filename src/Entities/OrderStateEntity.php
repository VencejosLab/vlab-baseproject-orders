<?php

namespace VLab\BaseOrders\Entities;

use CodeIgniter\Entity\Entity;

class OrderStateEntity extends Entity
{
    protected $attributes = [
        'id'          => 0,
        'order_id'    => null,
        'state'       => "",
        'company_id'  => null,
        'date'        => "",
        'created_by'  => "",
        'updated_by'  => "",
        'deleted_by'  => ""
    ];

    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [];
}
