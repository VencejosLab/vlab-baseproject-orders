<?php

namespace VLab\BaseOrders\Entities;

use CodeIgniter\Entity\Entity;

class SettingEntity extends Entity
{
    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [];

    protected $attributes = [
        'id'            => 0,
        'class'         => null,
        'key'           => null,
        'value'         => null,
        'type'          => null,
        'group'         => null,
        'sub_group'     => null,
        'company_id'    => null
    ];
}
