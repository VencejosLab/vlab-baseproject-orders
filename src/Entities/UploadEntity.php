<?php

namespace VLab\BaseOrders\Entities;

use CodeIgniter\Entity\Entity;

class UploadEntity extends Entity
{
    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [];

    protected $attributes = [
        'id'                => 0,
        'original_name'     => null,
        'new_name'          => null,
        'description'       => null,
        'path'              => null,
        'extension'         => null,
        'type'              => null,
        'id_type'           => null,
        'company_id'        => null
    ];
}
