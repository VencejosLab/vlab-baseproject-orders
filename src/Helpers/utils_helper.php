<?php

namespace VLab\BaseOrders\Helpers;

// app/Helpers/info_helper.php
use CodeIgniter\CodeIgniter;

/**
 * Returns CodeIgniter's version.
 */
function ci_version(): string
{
    return CodeIgniter::CI_VERSION;
}

/*
Recibe un string y limpia los caracteres especiales listados en $vowels
 */

function LimpiarCaracteres($cadena)
{
    $vowels = array("!", "¡", "_", '"', "'", "=", "@", ")", "<", ">", "(", "]", "[", "}", "{", "#", "*", ":", ";", "%", "/", "'\'", "¿", "?", "-", " ");
    $resultado = trim(str_replace($vowels, '', $cadena));
    return $resultado;
}

/**
 * Recibe un numero entero y lo formatea con formato 00,00
 * @param  $valor cualquier numero
 * @return $valor con el formato nuevo
 */
function FormatearNumero($valor)
{
    if (is_null($valor)) {
        exit("Util.FormatearMoneda: El valor pasado es nulo");
    }
    return number_format($valor, 2, ',', '.');
}

/**
 * Recibe un datetime y lo formatea para guardarlo en la DB con el formato yyyy-mm-dd
 * @param string $datetime fecha a guardar
 * @param string $formato
 * @return datetime
 */
function DatetimeDB($datetime, $formato = null)
{
    $result = date(is_null($formato) ? 'Y-m-d H:i:s' : $formato, strtotime(str_replace("/", "-", $datetime)));
    return $result;
}

/**
 * Construye un arbol a partir del arreglo en data
 */
function BuildTree($data, $rootId = 0)
{
    $tree = array(
        'children' => array(),
        'root' => array(),
    );
    foreach ($data as $ndx => $node) {
        $id = $node['id'];
        /* Puede que exista el children creado si los hijos entran antes que el padre */
        $node['children'] = (isset($tree['children'][$id])) ? $tree['children'][$id]['children'] : array();
        $tree['children'][$id] = $node;

        if ($node['padreid'] == $rootId) {
            $tree['root'][$id] = &$tree['children'][$id];
        } else {
            $tree['children'][$node['padreid']]['children'][$id] = &$tree['children'][$id];
        }
    }
    return $tree['root'];
}

/**
 *
 * Recibe un datetime de la DB y lo formatea para mostrarlo en el formato dd/mm/yyyy hh:mm
 */
function DBDatetime($datetime)
{
    if ($datetime == null) {
        return null;
    }
    $return = date("d/m/Y G:i", strtotime($datetime));
    return $return;
}

/**
 *
 *Recibe un datetime de la DB y lo formatea para mostrarlo en el formato dd/mm/yyyy
 */
function DBDate($datetime)
{
    if ($datetime == null) {
        return null;
    }
    $return = date("d/m/Y", strtotime($datetime));
    return $return;
}

function GenerarPassword()
{
    //Se define una cadena de caractares. Te recomiendo que uses esta.
    $cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
    //Obtenemos la longitud de la cadena de caracteres
    $longitudCadena = strlen($cadena);
    //Se define la variable que va a contener la contraseña
    $pass = "";
    //Se define la longitud de la contraseña
    $longitudPass = 6;

    //Creamos la contraseña
    for ($i = 1; $i <= $longitudPass; $i++) {
        //Definimos numero aleatorio entre 0 y la longitud de la cadena de caracteres-1
        $pos = rand(0, $longitudCadena - 1);
        //Vamos formando la contraseña en cada iteraccion del bucle, añadiendo a la cadena $pass la letra correspondiente a la posicion $pos en la cadena de caracteres definida.
        $pass .= substr($cadena, $pos, 1);
    }
    return $pass;
}

function CadenaRandom($longitud)
{
    //Se define una cadena de caractares. Te recomiendo que uses esta.
    $caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    //Obtenemos la longitud de la cadena de caracteres
    $totalCaracteres = strlen($caracteres);
    //Se define la variable que va a contener la contraseña
    $codigo = "";
    //Creamos la contraseña
    for ($i = 1; $i <= $longitud; $i++) {
        //Definimos numero aleatorio entre 0 y la longitud de la cadena de caracteres-1
        $pos = rand(0, $totalCaracteres - 1);
        //Vamos formando la contraseña en cada iteraccion del bucle, añadiendo a la cadena $pass la letra correspondiente a la posicion $pos en la cadena de caracteres definida.
        $codigo .= substr($caracteres, $pos, 1);
    }
    return $codigo;
}

function ObtenerUriString()
{
    $uriArray = explode("/", uri_string());
    $uriString = "";
    $area = $uriArray[0] == "Admin" || $uriArray[0] == "Cliente";
    if ($area) {
        $uriString = $uriArray[0]
            . (array_key_exists(1, $uriArray) ? "/$uriArray[1]" : null)
            . (array_key_exists(2, $uriArray) ? "/$uriArray[2]" : null)
            . (array_key_exists(3, $uriArray) ? "/*" : null);
    } else {
        $uriString = $uriArray[0]
            . (array_key_exists(1, $uriArray) ? "/$uriArray[1]" : null)
            . (array_key_exists(2, $uriArray) ? "/*" : null);
    }
    return $uriString;
}


function ArmarUrlAmigable($cadena)
{
    //Codificamos la cadena en formato utf8 en caso de que nos de errores
    //$cadena = utf8_encode($cadena);
    //Ahora reemplazamos las letras
    $cadena = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $cadena
    );
    $cadena = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $cadena
    );
    $cadena = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $cadena
    );
    $cadena = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $cadena
    );
    $cadena = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $cadena
    );
    $cadena = str_replace(array('ñ', 'Ñ', 'ç', 'Ç'), array('n', 'N', 'c', 'C'), $cadena);
    //si en la cadena hay un - reemplazo por un +
    $cadena = str_replace(array('-'), array('+'), $cadena);
    $cadena = LimpiarCaracteres($cadena);
    $cadena = str_replace(array(' '), array('-'), $cadena);
    $cadena = strtolower($cadena);
    return $cadena;
}

function floatvalue($val)
{
    $val = str_replace(",", ".", $val);
    $val = preg_replace('/.(?=.*.)/', '', $val);
    return floatval($val);
}
