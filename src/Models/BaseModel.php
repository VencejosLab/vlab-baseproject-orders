<?php

namespace VLab\BaseOrders\Models;

use CodeIgniter\Model;

class BaseModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'bases';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = [];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public function __construct(array $beforeInsertChild, array $afterInsertChild, array $beforeUpdateChild, array $afterUpdateChild, array $beforeDeleteChild, array $afterDeleteChild, array $beforeFindChild, array $afterFindChild)
    {
        //before's callbacks
        $this->beforeInsert = array_merge(['setCreatedBy', 'setCompanyId'], $beforeInsertChild);
        $this->beforeUpdate = array_merge(['setUpdatedBy'], $beforeUpdateChild);
        $this->beforeDelete = array_merge(['setDeletedBy'], $beforeDeleteChild);
        $this->beforeFind = array_merge([], $beforeFindChild);

        //after's callbacks
        $this->afterInsert = array_merge([], $afterInsertChild);
        $this->afterUpdate = array_merge([], $afterUpdateChild);
        $this->afterDelete = array_merge([], $afterDeleteChild);
        $this->afterFind = array_merge($afterFindChild);

        parent::__construct();
    }


    public  function multiple_result($sql)
    {
        if (empty($sql))
            return NULL;

        $i = 0;
        $set = [];


        if (mysqli_multi_query($this->db->connID, $sql)) {
            do {
                mysqli_next_result($this->db->connID);

                if (FALSE != $result = mysqli_store_result($this->db->connID)) {
                    $row_id = 0;

                    while ($row = $result->fetch_object()) {
                        $set[$i][$row_id] = $row;
                        $row_id++;
                    }
                }

                $i++;
            } while (mysqli_more_results($this->db->connID));
        }

        return $set;
    }

    /*
        limpia conexion actual y la reinicia para liberar los recursos.
     */
    function next_result()
    {

        if (is_object($this->conn_id)) {
            return mysqli_next_result($this->conn_id);
        }
    }

    function setCompanyId(array $data)
    {
        if (auth()->loggedIn()) {
            $data['data']['company_id'] = 1;
        }
        return $data;
    }

    function setCreatedBy(array $data)
    {
        if (auth()->loggedIn()) {
            $data['data']['created_by'] = auth()->user()->username;
        }
        return $data;
    }

    function setUpdatedBy(array $data)
    {
        if (auth()->loggedIn()) {
            $data['data']['updated_by'] = auth()->user()->username;
        }
        return $data;
    }

    function setDeletedBy(array $data)
    {
        if (auth()->loggedIn()) {
            if (sizeof($data['id']) == 1) {
                $original = $this->find($data['id'][0]);
                $original->deleted_by = auth()->user()->username;

                $this->update($data['id'][0], $original);
            }
        }
        return $data;
    }

    // public function setDeleted(array $data): array
    // {
    //     if ($data != null) {
    //         if(is_array($data['data'])){
    //             foreach ($data['data'] as $key => $value) {
    //                 $value->deleted = $value->deleted_at != null;
    //             }
    //         }else{
    //             $data['data']->deleted = $data['data']->deleted_at != null;
    //         }

    //         return $data;
    //     }
    // }
}
