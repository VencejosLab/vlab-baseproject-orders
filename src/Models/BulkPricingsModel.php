<?php

namespace VLab\BaseOrders\Models;

use VLab\BaseOrders\Entities\BulkPricingEntity;
use VLab\BaseOrders\Models\BaseModel;

class BulkPricingsModel extends BaseModel
{
    protected $DBGroup          = 'default';
    protected $table            = 'bulk_pricing';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = BulkPricingEntity::class;
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['id', 'product_id', 'min_bulk', 'max_bulk', 'discount',  'allow_select_qty', 'unit_sale_price', 'sale_price', 'valid_from', 'valid_until', 'active', 'company_id', 'created_by', 'updated_by', 'deleted_by'];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public function __construct()
    {
        parent::__construct($this->beforeInsert, $this->afterInsert, $this->beforeUpdate, $this->afterUpdate, $this->beforeDelete, $this->afterDelete, $this->beforeFind, $this->afterFind);
    }


       /**
     * Ejecuta la llamada el procedimiento almacenado para bucar los registros en la base de datos.
     * @return array
     */
    public function searchByProduct($productId, $companyId = 1)
    {
        $query = "CALL GetProductBulkPrices($companyId,$productId);";
        $result = $this->query($query)->getResult();
        return $result;
    }
    
}
