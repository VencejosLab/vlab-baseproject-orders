<?php

namespace VLab\BaseOrders\Models;

use VLab\BaseOrders\Entities\OrderDetailEntity;
use VLab\BaseOrders\Models\BaseModel;


class OrdersDetailsModel extends BaseModel
{
    protected $DBGroup          = 'default';
    protected $table            = 'order_details';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = OrderDetailEntity::class;
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['id', 'order_id', 'product_id', 'quantity', 'unit_price', 'subtotal', 'price_list_id', 'bulk_items', 'bulk_price', 'bulk_unit_price', 'confirmed_quantity', 'confirmed_subtotal',  'company_id', 'created_by', 'updated_by', 'deleted_by'];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public function __construct()
    {
        parent::__construct($this->beforeInsert, $this->afterInsert, $this->beforeUpdate, $this->afterUpdate, $this->beforeDelete, $this->afterDelete, $this->beforeFind, $this->afterFind);
    }

    public function search($orderId, $currency)
    {
        try {
            $companyId = session('company_id');
            $query = "CALL GetDetailsOrder($orderId, '$currency', $companyId);";
            $result = $this->db->query($query)->getResultArray();
            return $result;
        } catch (\Throwable $th) {
            log_message('error', $th->__toString());
            throw $th;
        }
    }
}
