<?php

namespace VLab\BaseOrders\Models;

use VLab\BaseOrders\Entities\OrderEntity;
use VLab\BaseOrders\Models\BaseModel;

class OrdersModel extends BaseModel
{
    protected $DBGroup          = 'default';
    protected $table            = 'orders';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = OrderEntity::class;
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['id', 'number', 'client_id', 'user_id', 'date', 'confirmation_date', 'delivery_date', 'shipping_date', 'shipping_status', 'free_shipping', 'client_fullname', 'estimate_id', 'currency_id', 'exchange_rate', 'discount_percentage', 'sale_condition_id', 'credit_term_id', 'notes', 'total', 'total_discount', 'total_taxes', 'total_net', 'company_id', 'created_by', 'updated_by', 'deleted_by'];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = ['afterFind'];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public function __construct()
    {
        parent::__construct($this->beforeInsert, $this->afterInsert, $this->beforeUpdate, $this->afterUpdate, $this->beforeDelete, $this->afterDelete, $this->beforeFind, $this->afterFind);
    }


    /**
     * Ejecuta la llamada el procedimiento almacenado para bucar los registros en la base de datos.
     * @return array
     */
    public function search($orderId = 0, $clientId = 0, $number = '',  $dateFrom = '', $dateUntil = '', $status = '', $currency = '', $orderBy = '', $pageIndex = 1, $pageSize = 10)
    {
        $companyId = session('company_id');
        $query = "CALL GetOrdersAdmin($orderId,"
            . "$companyId,"
            . "'$number',"
            . "$clientId,"
            . "'$dateFrom',"
            . "'$dateUntil',"
            . "'$status',"
            . "'$currency',"
            . "'$orderBy',"
            . "$pageIndex,"
            . "$pageSize,"
            . "@totalRecords,@firtsRecords,@lastRecords);";
        $this->next_result();
        $result = $this->multiple_result($query);
        //El sp no devolvio filas
        if (count($result) == 2) {
            $data["Rows"] = [];
        } else {
            $data["Rows"] = $result[0]; //filas devueltas por el sp
        }
        $data["Properties"] = $result[1]; //propiedades de columnas
        $data["TotalRecords"] = (int)($result[2][0])->totalRecords;

        return $data;
    }

    /**
     * Ejecuta la llamada el procedimiento almacenado para bucar los registros en la base de datos.
     * @return array
     */
    public function searchByClient($clientId = 0,  $dateFrom = '', $dateUntil = '', $status = '', $currency = '', $orderBy = '', $pageIndex = 1, $pageSize = 10)
    {
        $companyId = session('company_id');
        $query = "CALL GetOrdersClient($companyId,"
            . "$clientId,"
            . "'$dateFrom',"
            . "'$dateUntil',"
            . "'$status',"
            . "'$currency',"
            . "'$orderBy',"
            . "$pageIndex,"
            . "$pageSize,"
            . "@totalRecords,@firtsRecords,@lastRecords);";
        $result = $this->db->query($query)->getResult();

        $result =  sizeof($result) > 0 ? $result : [];

        // if (count($result) == 1) {
        //     $data["Rows"] = [];
        // } else {
        //     $data["Rows"] = $result[0]; //filas devueltas por el sp
        // }

        // $data["Rows"] = $result[0]; //filas devueltas por el sp
        // $data["TotalRecords"] = (int)($result[1][0])->totalRecords;

        return $result;
    }

    public function getWithDetails($orderId, $currency)
    {
        try {
            $order = $this->search($orderId, 0, '', '', '', '', $currency, 1, 1)["Rows"];
            if (sizeof($order) == 0) {
                throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
            }
            $detailsModel = new OrdersDetailsModel();

            $data = json_decode(json_encode($order[0]), true);
            $data['details'] =  $detailsModel->search($orderId, $currency);

            return $data;
        } catch (\Throwable $th) {
            log_message('error', $th->__toString());
            throw $th;
        }
    }

    public function process($id, $clientId, $userId, $confirmationDate, $deliveryDate, $shippingDate, $shippingStatus, $freeShipping, $clientFullname, $estimateId, $discountPercentage, $saleConditionId, $creditTermId, $notes, $status, $userName)
    {
        try {
            $companyId = session('company_id');
            $query = "CALL ProcessOrder($id,"
                . "$clientId,"
                . "$userId,"
                . "'$confirmationDate',"
                . "'$deliveryDate',"
                . "'$shippingDate',"
                . "'$shippingStatus',"
                . "'$freeShipping',"
                . "'$clientFullname',"
                . "$estimateId,"
                . "$discountPercentage,"
                . "$saleConditionId,"
                . "$creditTermId,"
                . "'$notes',"
                . "'$status',"
                . "$companyId,"
                . "'$userName');";

            $result = $this->db->query($query)->getResultArray()[0];

            return $result;
        } catch (\Throwable $th) {
            log_message('error', $th->__toString());
            throw $th;
        }
    }

    public function afterFind(array $data)
    {
        $modelOrderDetails = new OrdersDetailsModel();
        $data['data']->details = $modelOrderDetails->where('order_id', $data['id'])->findAll();

        return $data;
    }
}
