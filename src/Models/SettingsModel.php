<?php

namespace VLab\BaseOrders\Models;

use VLab\BaseOrders\Models\BaseModel;

class SettingsModel extends BaseModel
{
    protected $DBGroup          = 'default';
    protected $table            = 'settings';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['id', 'class', 'key', 'value', 'type', 'group', 'sub_group', 'company_id'];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public function __construct()
    {
        parent::__construct($this->beforeInsert, $this->afterInsert, $this->beforeUpdate, $this->afterUpdate, $this->beforeDelete, $this->afterDelete, $this->beforeFind, $this->afterFind);
    }

    public function search($group = "")
    {
        try {
            $companyId = session('company_id');
            $query = "CALL GetSettings($companyId,'$group');";
            $result = $this->db->query($query)->getResult('array');
            return $result;
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function getByKey(string $key)
    {
        try {
            if ($key == null || $key == '') return null;

            $key = trim(strtolower($key));

            $result = $this->where(['key' => $key, 'company_id' => session('company_id')])->first();

            return $result;
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function getByGroup(string $group)
    {
        try {
            if ($group == null || $group == '') return null;

            $group = trim(strtolower($group));

            $result = $this->where(['group' => $group, 'company_id' => session('company_id')])->findAll();

            return $result;
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
