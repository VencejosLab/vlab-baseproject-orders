<?= $this->extend('Admin/Layout/Main') ?>
<?= $this->section('content') ?>
<div id="app-form-bulkpricings" class="container-fluid"  data-aos="fade-up">
    <div class="card">
        <div class="card-header main">
            <h5 class="card-title">{{title}}</h5>
        </div>
        <ValidationForm @submit="save" :validation-schema="vschema" v-slot="{ errors, dirty, valid, invalid,  isSubmitting }">
            <div class="card-body">
               <!--  -->
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-6 d-flex justify-content-start">
                        <a href="#!" onclick="history.back()">Volver</a>
                    </div>
                    <div class="col-6 d-flex justify-content-end">
                        <button type="submit" :disabled="isSubmitting" :class="{'ld-ext-right': true, 'btn btn-success': true, 'running': isSubmitting }">
                            <i class="bi bi-check-lg"></i> Guardar
                            <div class="ld">
                                <img src="<?= base_url('dist/images/loading.gif'); ?>" style="font-size:1em" />
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        </ValidationForm>
    </div>
</div>
<?= $this->endSection(); ?>

<?= $this->section('scripts') ?>
<script type="text/javascript" src='<?= base_url("dist/js/admin/bulkpricings/form.js"); ?>'></script>
<?= $this->endSection('scripts') ?>