<?= $this->extend('Admin/Layout/Main') ?>
<?= $this->section('content') ?>
<div id="app-form-categories" class="container-fluid">
    <div class="card">
        <div class="card-header main">
            <h5 class="card-title">Eliminar registro</h5>
        </div>
        <ValidationForm @submit="remove" :validation-schema="vschema" v-slot="{ errors, dirty, valid, invalid,  isSubmitting }">
            <div class="card-body">
                <div class="alert alert-warning">
                    <h5>Atención</h5>
                    <p>¿Desea eliminar este registro?</p>
                </div>
                <input type="hidden" id="id" value="<?= $id; ?>">
                <div class="row">
                    <div class="col-4">
                        <textinput  name="name" disabled=true v-model="datavm.name" placeholder="Ingrese un nombre" label="Nombre"></textinput>
                    </div>
                    <div class="col-5">
                        <textinput  name="description" disabled=true v-model="datavm.description" placeholder="Ingrese una descripcion" label="Descripcion"></textinput>
                    </div>
                    <div class="col-3">
                        <customswitch name="deleted" v-model="datavm.deleted" label="Eliminado"></customswitch>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <label for="father_id" class="col-form-label">Categoría padre</label>
                        <treelist id="categories-tree" disabled=true  v-model="datavm.father_id" v-if="datavm.father_id != null || datavm.id == 0"></treelist>
                    </div>
                </div>
                <!-- <div class="col-12">
                        <genericselect name="father_id" v-model="datavm.father_id" label="Padre" url-path="/api/categories/select"></genericselect>
                    </div> -->
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-6 d-flex justify-content-start">
                        <a href="<?= base_url('admin/categories'); ?>">Volver al listado</a>
                    </div>
                    <div class="col-6 d-flex justify-content-end">
                        <button type="submit" :disabled="isSubmitting" :class="{'ld-ext-right': true, 'btn btn-danger': true, 'running': isSubmitting }">
                            <i class="bi bi-bi-trash-fill"></i> Aceptar
                            <div class="ld">
                                <img src="<?= base_url('dist/images/loading.gif'); ?>" style="font-size:1em" />
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        </ValidationForm>
    </div>
</div>
<?= $this->endSection(); ?>

<?= $this->section('scripts') ?>
<script type="text/javascript" src="<?= base_url("dist/js/admin/category/form.js"); ?>"></script>
<?= $this->endSection('scripts') ?>