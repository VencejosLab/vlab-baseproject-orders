<div id="app-form-categories" class="container-fluid">
    <div class="card">
        <div class="card-header main">
            <h5 class="card-title">{{title}}</h5>
        </div>
        <ValidationForm @submit="save" :validation-schema="vschema" v-slot="{ errors, dirty, valid, invalid,  isSubmitting }" ref="form-filters">
            <div class="card-body">
                <input type="hidden" id="id" v-model="id" name="id">
                <div class="row">
                    <div class="col-12">
                        <textinput name="name" v-model="datavm.name" placeholder="Ingrese un nombre" label="Nombre"></textinput>
                    </div>
                    <div class="col-12">
                        <customswitch name="show_in_landing" v-model="datavm.show_in_landing" label="Mostrar en landing"></customswitch>
                    </div>
                    <div class="col-12">
                        <customswitch name="show_in_menu" v-model="datavm.show_in_menu" label="Mostrar en menú"></customswitch>
                    </div>
                    <div class="col-12">
                        <textinput name="description" v-model="datavm.description" placeholder="Ingrese una descripcion" label="Descripcion"></textinput>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <label for="father_id" class="col-form-label">Categoría padre</label>
                        <treelist id="categories-tree" v-model="datavm.father_id" v-if="loaded"></treelist>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-6 d-flex justify-content-start">
                        <a href="#!" @click="clearForm">Cancelar</a>
                    </div>
                    <div class="col-6 d-flex justify-content-end">
                        <button type="submit" :disabled="isSubmitting" :class="{'ld-ext-right': true, 'btn btn-success': true, 'running': isSubmitting }">
                            <i class="bi bi-check-lg"></i> Guardar
                            <div class="ld">
                                <img src="<?= base_url('dist/images/loading.gif'); ?>" style="font-size:1em" />
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        </ValidationForm>
    </div>
</div>