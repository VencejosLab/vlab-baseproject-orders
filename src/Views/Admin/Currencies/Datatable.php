<div id="app-datatable-currencies" class="container-fluid">
    <datagrid ref="datagrid" :urlrequest="urlRequest" :title="title" :writeurlrequest="writeUrlRequest" :actions="actions">
    </datagrid>

    <modalform idmodal="modalCurrency" :vschema="vschema" :schema="formSchema" :formdata="datavm" ref="modalForm" @submit="save">
    <template v-slot:title>{{ datavm.id == 0 ? 'Nueva divisa' : 'Editar divisa' }}</template>
    </modalform>
</div>
<?= $this->section('scripts') ?>
<script type="text/javascript" src='<?= base_url("dist/js/admin/currencies/datatable.js"); ?>'></script>
<?= $this->endSection('scripts') ?>