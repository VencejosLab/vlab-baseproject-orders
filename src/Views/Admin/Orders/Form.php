<?= $this->extend('Admin/Layout/Main') ?>
<?= $this->section('content') ?>
<div id="app-form-orders" class="container-fluid">
    <div class="card" data-aos="fade-up">
        <div class="card-header main">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                    <h5 class="card-title">{{title}}</h5>
                </div>
                <div class="col-xl-8 col-lg-9 col-md-8 col-12 d-flex justify-content-end">
                    <h3>{{ datavm.number }}</h3>
                </div>
            </div>
        </div>
        <ValidationForm @submit="save" :validation-schema="vschema" v-slot="{ errors, dirty, valid, invalid,  isSubmitting }">
            <div class="card-body">
                <div v-if="errorMessages.length" class="alert alert-danger">
                    <ul>
                        <li v-for="(error, key) in errorMessages">{{error}}</li>
                    </ul>
                </div>
                <div class="card mb-3">
                    <div class="card-body">
                        <input type="hidden" id="id" name="id" value="<?= $id ?>">
                        <textinput id="user_id" name="user_id" v-model="datavm.user_id" type="hidden"></textinput>
                        <div class="row">
                            <div class="col-xl-2 col-lg-2 col-md-3 col-12">
                                <textinput name="date" type="text" v-model="datavm.date" label="Fecha" disabled="disabled"></textinput>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-3 col-12">
                                <genericselect name="client_id" :disabled="true" v-model="datavm.client_id" label="Cliente" url-path="/api/clients/select"></genericselect>
                            </div>
                            <div class="col-xl-2 col-lg-2 col-md-3 col-12">
                                <label for="status" class="col-form-label">Estado</label>
                                <select name="status" class="form-control" v-model="datavm.status">
                                    <option value="">--TODOS--</option>
                                    <option value="INPROGRESS">EN PROGRESO</option>
                                    <option value="ISSUED">EMITIDO</option>
                                    <option value="CONFIRMED">CONFIRMADO</option>
                                    <option value="CANCELLED">CANCELADO</option>
                                </select>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-3 col-12">
                                <label for="status" class="col-form-label">Tipo entrega</label>
                                <ul>
                                    <li>
                                        <div class="custom-radio">
                                            <input type="radio" id="flat_rate" name="shipping_method" value="delivery" class="custom-control-input" v-model="datavm.shipping_method" />
                                            <label class="custom-control-label" for="flat_rate">Envío a domicilio con expreso</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="custom-radio">
                                            <input type="radio" id="local_pickup" name="shipping_method" value="pickup" class="custom-control-input" v-model="datavm.shipping_method" />
                                            <label class="custom-control-label" for="local_pickup">Retiro en depósito</label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-xl-2 col-lg-2 col-md-3 col-12">
                                <textinput name="exchange_rate" type="text" v-model="datavm.exchange_rate" label="Tasa de cambio" disabled="disabled"></textinput>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-2 col-lg-2 col-md-3 col-12">
                                <textinput name="confirmation_date" type="date" v-model="datavm.confirmation_date" label="Fecha confirmación"></textinput>
                            </div>
                            <div class="col-xl-2 col-lg-2 col-md-3 col-12">
                                <textinput name="delivery_date" type="date" v-model="datavm.delivery_date" label="Fecha envío"></textinput>
                            </div>
                            <div class="col-xl-2 col-lg-2 col-md-3 col-12">
                                <textinput name="shipping_date" type="date" v-model="datavm.shipping_date" label="Fecha entrega"></textinput>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Details order -->
                <div class="card" id="orderDetailsCard">
                    <div class="card-body">
                        <div class="row d-flex justify-content-end">
                            <div class="col-12">
                                <p>Items: <b>{{ datavm.items }}</b></p>
                            </div>
                        </div>
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr class="text-center">
                                    <th>#</th>
                                    <!-- <th></th> -->
                                    <th>Producto</th>
                                    <!-- <th>Bulto</th> -->
                                    <th>Cantidad</th>
                                    <th>P. Unidad</th>
                                    <th>Subtotal</th>
                                    <th>Confirmado</th>
                                    <th>Subtotal confirmado</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(item, index) in datavm.details" :key="index">
                                    <td>{{ item.rn }}</td>
                                    <!-- <td>
                                        <img :src=" item.product_image_url ? item.product_image_url : '/dist/images/not-image.svg'" class="img-fluid" width="200" height="200">
                                    </td> -->
                                    <td>
                                        <div>
                                            <div class="font-weight-bold">{{ item.product_name }}</div>
                                            <div class="small text-gray-600">Cod:
                                                <a :href="`/admin/products/edit/${item.product_id}`" target="_blank">{{ item.product_code }}</a>
                                            </div>
                                        </div>
                                    </td>
                                    <!-- <td>
                                        <template v-if="item.bulk_quantity > 1">
                                            <span> x {{ item.bulk_quantity }} unidades </span> <br>
                                            <span> P. bulto: {{ item.bulk_price }} </span>
                                        </template>
                                        <template v-else>-</template>
                                    </td> -->
                                    <td class="text-center">
                                        <template v-if="item.bulk_items > 1">
                                            <span>{{ item.quantity_format }} bulto(s)</span><br>
                                            <span>Unidades: <b>{{ item.bulk_items_format }}</b></span><br>
                                            <span>P. unidad: <b>{{ item.bulk_unit_price_format }}</b></span>
                                        </template>
                                        <template v-else>
                                            <span> {{ `${item.quantity_format} unidad(es)` }}</span>
                                        </template>
                                    </td>
                                    <td> {{ item.unit_price_format }} </td>
                                    <td> {{ item.subtotal_format }} </td>
                                    <td>
                                        <textinput :id="`qty_confirmed_${item.rn}`" :name="`qty_confirmed_${item.rn}`" v-model="item.confirmed_quantity" type="number" @update:model-value="calculateSubtotalConfirmed(item.id)">
                                        </textinput>
                                    </td>
                                    <td>
                                        {{ formatterCurrency(item.confirmed_subtotal) }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- End Details order -->

                <!-- Totals -->
                <div class="row mt-3" id="orderTotalsCard">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row d-flex justify-content-end">
                                    <div class="col-12 d-flex justify-content-end">
                                        <div id="totalOrderOriginal">
                                            <h4> Total pedido: {{ formatterCurrency(datavm.original_total_net, datavm.currency) }} </h4>
                                        </div>
                                    </div>
                                    <div class="col-12 d-flex justify-content-end">
                                        <div id="totalConfirmedOrderOriginal">
                                            <h4> Total confirmado: {{ formatterCurrency(original_total_confirmed, datavm.currency) }} </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-6 col-lg-6 col-md-6 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row d-flex justify-content-end">
                                    <div class="col-12 d-flex justify-content-end">
                                        <div id="totalOrder">
                                            <h4> Total pedido: {{ formatterCurrency(datavm.total_net, datavm.currencyQuery) }} </h4>
                                        </div>
                                    </div>
                                    <div class="col-12 d-flex justify-content-end">
                                        <div id="totalConfirmedOrder">
                                            <h4> Total confirmado: {{ formatterCurrency(total_confirmed, datavm.currencyQuery) }} </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Totals -->

                <!-- Notes -->
                <div class="row d-flex justify-content-end">
                    <div class="col-6">
                        <label for="notes" class="col-form-label">Notas</label>
                        <textarea class="form-control" v-model="datavm.notes">
                        </textarea>
                    </div>
                </div>
                <!-- End notes -->
            </div>


            <div class="card-footer">
                <div class="row">
                    <div class="col-6 d-flex justify-content-start">
                        <a href="<?= base_url('admin/orders'); ?>">Volver al listado</a>
                    </div>
                    <div class="col-6 d-flex justify-content-end">
                        <button type="submit" :disabled="isSubmitting" :class="{'ld-ext-right': true, 'btn btn-success': true, 'running': isSubmitting }">
                            <i class="bi bi-check-lg"></i> Guardar y volver
                            <div class="ld">
                                <img src="<?= base_url('dist/images/loading.gif'); ?>" style="font-size:1em" />
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        </ValidationForm>
    </div>
</div>
<?= $this->endSection(); ?>

<?= $this->section('scripts') ?>
<script type="text/javascript" src="<?= base_url("dist/js/admin/orders/form.js"); ?>"></script>
<?= $this->endSection('scripts') ?>