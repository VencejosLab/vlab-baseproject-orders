<?= $this->extend('Admin/Layout/Main') ?>
<?= $this->section('content') ?>
<div id="app-form-products" class="container-fluid">
    <div class="card" data-aos="fade-up">
        <div class="card-header main">
            <div class="row">
                <div class="col-xl-9 col-lg-9 col-md-9 col-12">
                    <h5 class="card-title">{{title}}</h5>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-12 justify-content-end d-flex" v-if="action == 'update'">
                    <a href="<?= base_url('admin/products/new'); ?>" class="btn btn-secondary">
                        <i class="bi bi-plus"></i> Nuevo producto
                    </a>
                </div>
            </div>

        </div>
        <ValidationForm @submit="save" :validation-schema="vschema" v-slot="{ errors, dirty, valid, invalid,  isSubmitting }">
            <div class="card-body">
                <h5>{{ datavm.name }} ({{ datavm.code }})</h5>
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" href="#main" data-toggle="tab" data-target="#main">Principal</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#stock" data-toggle="tab" data-target="#stock">Stock y cantidades</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#prices" data-toggle="tab" data-target="#prices">Precios</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" v-if="action == 'update'" href="#features" data-toggle="tab" data-target="#features">Características</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" v-if="action == 'update'" href="#images" data-toggle="tab" data-target="#images">Fotos</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="main" role="tabpanel" aria-labelledby="main-tab">
                        <div class="p-3">
                            <input type="hidden" id="id" value="<?= $id; ?>">
                            <div class="row">
                                <div class="col-3">
                                    <textinput name="code" v-model="datavm.code" placeholder="Ingrese un código" label="Código"></textinput>
                                </div>
                                <div class="col-7">
                                    <textinput name="name" v-model="datavm.name" placeholder="Ingrese un nombre" label="Nombre"></textinput>
                                </div>
                                <div class="col-2">
                                    <customswitch name="active" v-model="datavm.active" label="Activo"></customswitch>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <textareaeditor v-model="datavm.description" name="description" label="Descripcion"></textareaeditor>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3">
                                    <genericselect name="category_id" v-model="datavm.category_id" label="Categoría" url-path="/api/categories/select"></genericselect>
                                </div>
                                <div class="col-3">
                                    <genericselect name="brand_id" v-model="datavm.brand_id" label="Marca" url-path="/api/brands/select"></genericselect>
                                </div>
                                <!-- <div class="col-3">
                                    <genericselect name="supplier_id" v-model="datavm.supplier_id" label="Proveedor" url-path="/api/suppliers/select"></genericselect>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="stock" role="tabpanel" aria-labelledby="stock-tab">
                        <div class="p-3">
                            <div class="row">
                                <div class="col-3">
                                    <textinput name="stock" type="number" v-model="datavm.stock" label="Stock actual"></textinput>
                                </div>
                                <div class="col-3">
                                    <textinput name="min_stock" type="number" v-model="datavm.min_stock" label="Stock min."></textinput>
                                </div>
                                <div class="col-3">
                                    <textinput name="min_package" type="number" v-model="datavm.min_package" label="Cantidad min. para venta"></textinput>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="prices" role="tabpanel" aria-labelledby="prices-tab">
                        <div class="p-3">
                            <div class="row">
                                <div class="col-3">
                                    <moneyinput label="Costo compra" :symbol="defaultCurrency" name="purchase_price" v-model="datavm.purchase_price"></moneyinput>
                                </div>
                                <div class="col-3">
                                    <moneyinput label="P. venta" :symbol="defaultCurrency" name="sale_price" v-model="datavm.sale_price"></moneyinput>
                                </div>
                                <div class="col-3">
                                    <moneyinput label="P. venta unidad" :symbol="defaultCurrency" name="sale_price_unit" v-model="datavm.sale_price_unit"></moneyinput>
                                </div>
                            </div>
                            <hr>
                            <div v-if="action == 'update'">
                                <div v-if="datavm.bulk_pricing.length > 0">
                                    <div class="row mb-2">
                                        <div class="col-12">
                                            <button class="btn btn-primary" type="button" @click="openModalBulkPrice(0, 'create')">
                                                <i class="bi bi-plus-circle"></i> Agregar precio por bulto
                                            </button>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover">
                                            <thead class="text-center">
                                                <tr>
                                                    <th>Desde</th>
                                                    <th>Precio</th>
                                                    <th>Precio unidad</th>
                                                    <th>Periodo</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(item, key) in datavm.bulk_pricing" :key="key">
                                                    <td>
                                                        {{item.max_bulk}} unidades <br>
                                                        <a href="#" class="small" @click="openModalBulkPrice(item.id, 'update')">
                                                            <i class="bi bi-pencil"></i> Editar
                                                        </a>
                                                        <a href="#" class="small text-danger" @click="openModalBulkPrice(item.id, 'delete')">
                                                            <i class="bi bi-trash"></i> Borrar
                                                        </a>
                                                    </td>
                                                    <td>$ {{item.sale_price}}</td>
                                                    <td>$ {{item.unit_sale_price}}</td>
                                                    <td>{{item.period}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div v-else>
                                    <div class="row">
                                        <div class="col-12 d-flex justify-content-center">
                                            <button class="btn btn-primary" type="button" @click="openModalBulkPrice(0, 'create')">
                                                <i class="bi bi-plus-circle"></i> Agregar precio por bulto
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" v-if="action == 'update'" id="features" role="tabpanel" aria-labelledby="features-tab">
                        <div class="p-3">
                            <div class="row">
                                <div class="col-12">
                                    <button class="btn btn-primary" type="button" @click="openModalFeature(0, 'create')">
                                        <i class="bi bi-plus-circle"></i> Agregar característica
                                    </button>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr class="text-center">
                                                    <th>Característica</th>
                                                    <th>Valor</th>
                                                    <th>Valor personalizado</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(item, key) in datavm.product_features" :key="key">
                                                    <td>
                                                        {{ item.feature }} <br>
                                                        <a href="#" class="small" @click="openModalFeature(item.id, 'update')">
                                                            <i class="bi bi-pencil"></i> Editar
                                                        </a>
                                                        <a href="#" class="small text-danger" @click="openModalFeature(item.id, 'delete')">
                                                            <i class="bi bi-trash"></i> Borrar
                                                        </a>
                                                    </td>
                                                    <td>
                                                        {{ item.value }}
                                                    </td>
                                                    <td>
                                                        {{ item.custom_value }}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" v-if="action == 'update'" id="images" role="tabpanel" aria-labelledby="images-tab">

                        <div class="row mb-3">
                            <div class="col-12">
                                <div class="progress">
                                    <div id="file-progress-bar" class="progress-bar"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="uploadResult"></div>
                        <div class="row mb-3">
                            <div class="col-12">
                                <input type="file" accept="image/*" ref="file" class="form-control validator" autocomplete="off" name="files[]" id="files" multiple @change="uploadPhotos">
                            </div>
                        </div>

                        <table class="table table-bordered table-light table-primary" v-if="datavm.photos.length > 0">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Imagen</th>
                                    <th>Nombre</th>
                                    <th>Extensión</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(photo, index) in datavm.photos" :key="index">
                                    <td>{{ index+1 }}</td>
                                    <td class="text-center">
                                        <img :src="`/api/file/${photo.new_name}`" class="img-fluid" alt="" style="max-width: 150px;"><br>
                                        <a href="#!" class="link small text-danger" @click="removePhoto(photo.id)">
                                            <i class="bi bi-trash"></i> Eliminar
                                        </a>
                                        <a :href="`/api/file/${photo.new_name}`" target="_blank" class="link small">
                                            <i class="bi bi-eye"></i> Ver
                                        </a>
                                    </td>
                                    <td>{{ photo.new_name }}</td>
                                    <td>{{ photo.extension }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-6 d-flex justify-content-start">
                        <a href="<?= base_url('admin/products'); ?>">Volver al listado</a>
                    </div>
                    <div class="col-6 d-flex justify-content-end">
                        <button type="submit" :disabled="isSubmitting" :class="{'ld-ext-right': true, 'btn btn-success': true, 'running': isSubmitting }">
                            <i class="bi bi-check-lg"></i> Guardar y volver
                            <div class="ld">
                                <img src="<?= base_url('dist/images/loading.gif'); ?>" style="font-size:1em" />
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        </ValidationForm>
    </div>
    <bulkpricemodal v-if="action == 'update'" ref="modalBulkPrice" :product_id="datavm.id" :default_currency="defaultCurrency" @close="refreshBulkPrices"></bulkpricemodal>
    <productfeaturemodal v-if="action == 'update'" ref="modalProductFeature" :product_id="datavm.id" @close="refreshFeatures"></productfeaturemodal>
    <dialogconfirmation :id="'modalDeletePhoto'" ref="modalDeletePhoto">
        <template v-slot:title> Eliminar photo </template>
        <template v-slot:body> ¿Desea eliminar esta foto del producto?.</template>
    </dialogconfirmation>
</div>
<?= $this->endSection(); ?>

<?= $this->section('scripts') ?>
<script type="text/javascript" src="<?= base_url("dist/js/admin/products/form.js"); ?>"></script>
<?= $this->endSection('scripts') ?>