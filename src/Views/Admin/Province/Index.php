<?php
$router = service('router');
$data['controller']  = $router->controllerName();
$data['method'] = $router->methodName();
$data['module'] = strtolower("Province");
$data['area']   = strtolower("Admin");
?>
<?= $this->extend('Admin/Layout/Main') ?>
<?= $this->section('content') ?>
<div data-aos="fade-up">
    <?= $this->include('Admin/Province/Datatable', $data) ?>
</div>
<?= $this->endSection(); ?>