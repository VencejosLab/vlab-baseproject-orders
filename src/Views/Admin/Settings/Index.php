<?php
$router = service('router');
$data['controller']  = $router->controllerName();
$data['method'] = $router->methodName();
$data['module'] = "Settings";
$data['area']   = strtolower("Admin");
?>
<?= $this->extend('Admin/Layout/Main') ?>
<?= $this->section('content') ?>
<div class="container-fluid">
    <div class="card" id="app-settings">
        <div class="card-header">
            <h5 class="card-title">Ajustes</h5>
        </div>
        <ValidationForm @submit="save" :validation-schema="vschema" v-slot="{ errors, dirty, valid, invalid,  isSubmitting }">
            <div class="card-body">
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active" id="pills-generals-tab" data-toggle="pill" data-target="#pills-generals" type="button" role="tab" aria-controls="pills-generals" aria-selected="true">Generales</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="pills-products-tab" data-toggle="pill" data-target="#pills-products" type="button" role="tab" aria-controls="pills-products" aria-selected="false">Productos</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="pills-currencies-tab" data-toggle="pill" data-target="#pills-currencies" type="button" role="tab" aria-controls="pills-currencies" aria-selected="false">Monedas y divisas</button>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-generals" role="tabpanel" aria-labelledby="pills-generals-tab" v-if="loaded">
                        <?= view('Admin/Settings/Partials/Generals') ?>
                    </div>
                    <div class="tab-pane fade" id="pills-currencies" role="tabpanel" aria-labelledby="pills-currencies-tab">
                        <?= view('Admin/Settings/Partials/Currencies') ?>
                    </div>
                    <div class="tab-pane fade" id="pills-products" role="tabpanel" aria-labelledby="pills-products-tab" v-if="loaded">
                        <?= view('Admin/Settings/Partials/Products') ?>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-6 d-flex justify-content-start">
                        <a href="#!" onclick="history.back()">Volver</a>
                    </div>
                    <div class="col-6 d-flex justify-content-end">
                        <button type="submit" :disabled="isSubmitting" :class="{'ld-ext-right': true, 'btn btn-success': true, 'running': isSubmitting }">
                            <i class="bi bi-check-lg"></i> Guardar
                            <div class="ld">
                                <img src="<?= base_url('dist/images/loading.gif'); ?>" style="font-size:1em" />
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        </ValidationForm>
    </div>
</div>

<?= $this->endSection(); ?>

<?= $this->section('scripts') ?>
<script type="text/javascript" src='<?= base_url("dist/js/admin/settings/form.js"); ?>'></script>
<?= $this->endSection('scripts') ?>