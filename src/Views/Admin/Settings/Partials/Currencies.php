<div class="card">
    <div class="card-body">
        <div class="alert alert-info">
            Las monedas marcadas como activas se mostrarán para selección en el catálogo de clientes.
        </div>
        <datagrid ref="datagridcurrency" urlrequest="api/currencies" title="currencies" writeurlrequest="false" :actions="actionsCurrencies">
        </datagrid>

        <modalform idmodal="modalCurrency" :vschema="vschemaCurrency" :schema="formSchemaCurrency" :formdata="datavmCurrency" ref="modalFormCurrency" @submit="saveCurrency">
            <template v-slot:title>{{ datavmCurrency.id == 0 ? 'Nueva divisa' : 'Editar divisa' }}</template>
        </modalform>
    </div>
</div>