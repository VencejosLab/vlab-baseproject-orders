<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-3 col-12">
                <genericselect name="default-admin-currency" url-path="/api/currencies/select" label="Moneda panel admin" v-model="datavm.generals.default_currency_admin[0].value"></genericselect>
                <span class="small">
                    Moneda por defecto utilizada para en el panel de administración.
                </span>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-12">
                <genericselect name="default-catalog-currency" url-path="/api/currencies/select" label="Moneda catálogo" v-model="datavm.generals.default_currency[0].value"></genericselect>
                <span class="small">
                    Moneda por defecto utilizada para mostrar los precios en el catálogo de clientes.
                </span>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-12">
                <moneyinput name="min-purchase-mount" v-model="datavm.generals.min_purchase_mount[0].value" placeholder="" label="Monto mínimo de compra"></moneyinput>
            </div>
        </div>
    </div>
</div>