<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-3 col-12">
                <customswitch v-model="datavm.products.show_sale_price[0].value" name="show_sale_price" label="Mostrar precios en catálogo"></customswitch>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-12">
                <customswitch v-model="datavm.products.check_stock[0].value" name="check_stock" label="Control de stock"></customswitch>
            </div>
        </div>
    </div>
</div>