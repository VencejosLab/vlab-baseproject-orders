<div id="app-datatable-shippings" class="container-fluid">
    <div class="card">
        <div class="card-header main">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-9">
                    <h5>{{title}}</h5>
                </div>
                <div class="col-xl-8 col-lg-8 col-md-8 col-3 d-flex justify-content-end">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle text-white" data-toggle="dropdown">
                            <i class="bi bi-three-dots-vertical"></i>
                        </button>
                        <ul class="dropdown-menu pull-left" role="menu">
                            <li>
                                <a class="dropdown-item" href="Exportar">
                                    <i class="bi bi-download"></i> Exportar
                                </a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="Importar">
                                    <i class="bi bi-upload"></i> Importar
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <datagrid ref="datagrid" :urlrequest="urlRequest" :title="title" :writeurlrequest="writeUrlRequest" :actions="actions">
            </datagrid>
        </div>
    </div>
</div>
<?= $this->section('scripts') ?>
    <script type="text/javascript" src='<?= base_url("dist/js/admin/shippings/datatable.js"); ?>'></script>
<?= $this->endSection('scripts') ?>