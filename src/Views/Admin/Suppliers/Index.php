<?php
$router = service('router');
$data['controller']  = $router->controllerName();
$data['method'] = $router->methodName();
$data['module'] = "Suppliers";
$data['area']   = strtolower("Admin");
?>
<?= $this->extend('Admin/Layout/Main') ?>
<?= $this->section('content') ?>
<div data-aos="fade-up">
    <?= view('Admin/Suppliers/Datatable', $data) ?>
</div>
<?= $this->endSection(); ?>