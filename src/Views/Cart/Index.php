<?= $this->extend('Catalogue/Layout/Main') ?>
<?= $this->section('content') ?>

<main class="main cart-page">
    <nav class="breadcrumb-nav">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="<?= base_url(); ?>"><i class="d-icon-home"></i></a></li>
                <li>Carrito</li>
            </ul>
        </div>
    </nav>
    <div class="page-content pt-7 pb-10" v-if="cartStorage.items.length > 0">
        <!-- <div class="step-by pr-4 pl-4">
            <h3 class="title title-simple title-step active"><a href="cart.html">1. Shopping Cart</a></h3>
            <h3 class="title title-simple title-step"><a href="checkout.html">2. Checkout</a></h3>
            <h3 class="title title-simple title-step"><a href="order.html">3. Order Complete</a></h3>
        </div> -->
        <div class="container mt-2 mb-2">
            <div class="row">
                <div class="col-lg-8 col-md-12 pr-lg-4">
                    <cart-details :default-currency="settingsStorage.getCurrency.currency" :min-purchase-mount="settingsStorage.getMinPurchaseMount"></cart-details>
                    <div class="cart-actions mb-6 pt-4">
                        <a href="<?= base_url('catalogue') ?>" class="btn btn-dark btn-md btn-rounded btn-icon-left mr-4 mb-4"><i class="d-icon-arrow-left"></i>Volver al catálogo</a>
                        <button type="submit" class="btn btn-outline btn-dark btn-md btn-rounded">
                            Actualizar pedido
                        </button>
                    </div>
                    <div class="cart-coupon-box mb-8">
                        <h4 class="title coupon-title text-uppercase ls-m">Cupón de descuento</h4>
                        <input type="text" name="coupon_code" class="input-text form-control text-grey ls-m mb-4" id="coupon_code" value="" placeholder="Ingrese su cupón...">
                        <button type="submit" class="btn btn-md btn-dark btn-rounded btn-outline">Aplicar cupón</button>
                    </div>
                </div>
                <aside class="col-lg-4 sticky-sidebar-wrapper">
                    <cart-resumen :default-currency="settingsStorage.getCurrency.currency" :min-purchase-mount="settingsStorage.getMinPurchaseMount"></cart-resumen>
                </aside>
            </div>
        </div>
    </div>
    <div v-else class="page-content cart-empty">
        <div class="container">
            <div class="main-content">
                <i class="d-icon-bag cart-icon"></i>
                <h2 class="cart-descri">No hay productos cargados en su pedido</h2>
                <a href="<?= base_url('catalogue') ?>" class="btn btn-primary btn-rounded">
                    IR AL CATÁLOGO
                </a>
            </div>
        </div>
    </div>
</main>
<!-- End of Main Content -->
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script type="text/javascript" src="<?= base_url("dist/js/catalogue/main.js"); ?>"></script>
<?= $this->endSection('scripts') ?>