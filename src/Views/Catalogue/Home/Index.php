<?= $this->extend('Catalogue/Layout/Main') ?>
<?= $this->section('content') ?>
<main class="main">
    <?= $this->include('/Catalogue/Home/_Banner') ?>
    <!-- End PageHeader -->
    <div class="page-content mb-10 pb-2">
        <div class="container">
            <div class="row main-content-wrap gutter-lg" id="app-catalogue">
                <!-- Start sidebar filters -->
                <aside class="col-lg-3 sidebar sidebar-fixed sidebar-toggle-remain shop-sidebar sticky-sidebar-wrapper">
                    <div class="sidebar-overlay"></div>
                    <a class="sidebar-close" href="#"><i class="d-icon-times"></i></a>
                    <div class="sidebar-content">
                        <div class="sticky-sidebar" data-sticky-options="{'top': 10}">
                            <div class="filter-actions mb-4">
                                <a href="#" class="sidebar-toggle-btn toggle-remain btn btn-outline btn-primary btn-rounded btn-icon-right">Filter<i class="d-icon-arrow-left"></i></a>
                                <a href="#" class="filter-clean" @click="clearFilters" v-if="checkedCategories.length > 0">Limpiar</a>
                            </div>
                            <div class="widget widget-collapsible">
                                <h3 class="widget-title">Categorias</h3>
                                <div class="widget-body">
                                    <categorieslist :id="'categories-tree'" v-model="checkedCategories" v-if="loaded" ref="categories"></categorieslist>
                                </div>
                            </div>
                            <!--
                            <div class="widget widget-products">
                                <h4 class="widget-title mb-0 lh-1 border-no text-capitalize ">Destacados</h4>
                                <div class="widget-body">
                                    <div class="owl-carousel owl-nav-top" data-owl-options="{
												'items': 1,
												'loop': true,
												'nav': true,
												'dots': false,
												'margin': 20											
											}">
                                        <div class="products-col">
                                            <div class="product product-list-sm">
                                                <figure class="product-media" style="background-color: #f5f5f5;">
                                                    <a href="demo4-product.html">
                                                        <img src="images/shop/product-widget1.jpg" alt="product" width="100" height="114">
                                                    </a>
                                                </figure>
                                                <div class="product-details">
                                                    <h3 class="product-name">
                                                        <a href="demo4-product.html">Fashionable Orginal
                                                            Trucker</a>
                                                    </h3>
                                                    <div class="product-price">
                                                        <span class="price">$78.64</span>
                                                    </div>
                                                    <div class="ratings-container">
                                                        <div class="ratings-full">
                                                            <span class="ratings" style="width:40%"></span>
                                                            <span class="tooltiptext tooltip-top"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product product-list-sm">
                                                <figure class="product-media" style="background-color: #f5f5f5;">
                                                    <a href="demo4-product.html">
                                                        <img src="images/shop/product-widget2.jpg" alt="product" width="100" height="100">
                                                    </a>
                                                </figure>
                                                <div class="product-details">
                                                    <h3 class="product-name">
                                                        <a href="demo4-product.html">Men Summer Sneaker</a>
                                                    </h3>
                                                    <div class="product-price">
                                                        <span class="price">$79.45</span>
                                                    </div>
                                                    <div class="ratings-container">
                                                        <div class="ratings-full">
                                                            <span class="ratings" style="width:60%"></span>
                                                            <span class="tooltiptext tooltip-top"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product product-list-sm">
                                                <figure class="product-media" style="background-color: #f5f5f5;">
                                                    <a href="demo4-product.html">
                                                        <img src="images/shop/product-widget3.jpg" alt="product" width="100" height="100">
                                                    </a>
                                                </figure>
                                                <div class="product-details">
                                                    <h3 class="product-name">
                                                        <a href="demo4-product.html">Season Sports Cap</a>
                                                    </h3>
                                                    <div class="product-price">
                                                        <span class="price">$64.27</span>
                                                    </div>
                                                    <div class="ratings-container">
                                                        <div class="ratings-full">
                                                            <span class="ratings" style="width:20%"></span>
                                                            <span class="tooltiptext tooltip-top"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="products-col">
                                            <div class="product product-list-sm">
                                                <figure class="product-media" style="background-color: #f5f5f5;">
                                                    <a href="demo4-product.html">
                                                        <img src="images/shop/product-widget1.jpg" alt="product" width="100" height="114">
                                                    </a>
                                                </figure>
                                                <div class="product-details">
                                                    <h3 class="product-name">
                                                        <a href="demo4-product.html">Fashionable Orginal
                                                            Trucker</a>
                                                    </h3>
                                                    <div class="product-price">
                                                        <span class="price">$78.64</span>
                                                    </div>
                                                    <div class="ratings-container">
                                                        <div class="ratings-full">
                                                            <span class="ratings" style="width:40%"></span>
                                                            <span class="tooltiptext tooltip-top"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product product-list-sm">
                                                <figure class="product-media" style="background-color: #f5f5f5;">
                                                    <a href="demo4-product.html">
                                                        <img src="images/shop/product-widget2.jpg" alt="product" width="100" height="100">
                                                    </a>
                                                </figure>
                                                <div class="product-details">
                                                    <h3 class="product-name">
                                                        <a href="demo4-product.html">Men Summer Sneaker</a>
                                                    </h3>
                                                    <div class="product-price">
                                                        <span class="price">$79.45</span>
                                                    </div>
                                                    <div class="ratings-container">
                                                        <div class="ratings-full">
                                                            <span class="ratings" style="width:60%"></span>
                                                            <span class="tooltiptext tooltip-top"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product product-list-sm">
                                                <figure class="product-media" style="background-color: #f5f5f5;">
                                                    <a href="demo4-product.html">
                                                        <img src="images/shop/product-widget3.jpg" alt="product" width="100" height="100">
                                                    </a>
                                                </figure>
                                                <div class="product-details">
                                                    <h3 class="product-name">
                                                        <a href="demo4-product.html">Season Sports Cap</a>
                                                    </h3>
                                                    <div class="product-price">
                                                        <span class="price">$64.27</span>
                                                    </div>
                                                    <div class="ratings-container">
                                                        <div class="ratings-full">
                                                            <span class="ratings" style="width:20%"></span>
                                                            <span class="tooltiptext tooltip-top"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </aside>
                <!-- End sidebar filters -->
                <div class="col-lg-9 main-content">
                    <!-- Start Toolbok -->
                    <nav class="toolbox sticky-toolbox sticky-content fix-top">
                        <div class="toolbox-left">
                            <a href="#" class="toolbox-item left-sidebar-toggle btn btn-sm btn-outline btn-primary btn-rounded d-lg-none">Filtros<i class="d-icon-arrow-right"></i></a>
                            <div class="toolbox-item">
                                <div class="header-search hs-simple">
                                    <form action="#" class="input-wrapper">
                                        <input type="text" class="form-control" v-model="filters.search" name="search" autocomplete="off" placeholder="Buscar..." required />
                                        <button class="btn btn-search" type="button" @click="getProducts" title="submit-button">
                                            <i class="d-icon-search"></i>
                                        </button>
                                    </form>
                                </div>
                            </div>
                            <!-- End Header Search -->
                            <div class="toolbox-item toolbox-sort select-box">
                                <label>Ordenar :</label>
                                <select name="orderby" class="form-control" v-model="orderBy" @change="getProducts">
                                    <option value="">Por defecto</option>
                                    <option value="A-Z">A - Z</option>
                                    <option value="Z-A">Z - A</option>
                                    <option value="min_price">Menor precio</option>
                                    <option value="max_price">Mayor precio</option>
                                </select>
                            </div>
                            <div class="toolbox-item toolbox-sort select-box">
                                <label>Moneda: </label>
                                <select name="currency" class="form-control" v-model="filters.currency" @change="getProducts">
                                    <option value="USD">USD</option>
                                    <option value="ARS">ARS</option>
                                </select>
                            </div>
                        </div>
                        <div class="toolbox-right">
                            <!-- <div class="toolbox-item toolbox-show select-box">
                                <label>Mostrar :</label>
                                <select name="pageSize" class="form-control" v-model="pagination.pageSize">
                                    <option value="12">12</option>
                                    <option value="24">24</option>
                                    <option value="36">36</option>
                                </select>
                            </div> -->
                            <div class="toolbox-item toolbox-layout">
                                <a href="#!" :class="`d-icon-mode-list btn-layout ${modeView == 'list' ? 'active' : ''}`" @click="modeView = 'list'"></a>
                                <a href="#!" :class="`d-icon-mode-grid btn-layout ${modeView == 'grid' ? 'active' : ''}`" @click="modeView = 'grid'"></a>
                            </div>
                        </div>
                    </nav>
                    <!-- End Catalogue  -->
                    <!-- Start Catalogue -->
                    <div :class="modeView == 'list' ? 'product-lists product-wrapper' :  'row cols-2 cols-sm-3 product-wrapper'">
                        <productcatalogue v-for="(product, index) in products" :data-product="product" :currency="filters.currency" :mode-view="modeView">
                        </productcatalogue>
                    </div>
                    <!-- End Catalogue -->

                </div>
            </div>
        </div>
    </div>
</main>
<!-- End of Main Content -->
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script type="text/javascript" src="<?= base_url("dist/js/catalogue/main.js"); ?>"></script>
<?= $this->endSection('scripts') ?>