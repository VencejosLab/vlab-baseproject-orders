<?= $this->extend('Catalogue/Layout/Main') ?>
<?= $this->section('content') ?>
<main class="main">
    <!-- End PageHeader -->
    <div class="page-content mb-10 pb-2">
        <div class="container d-flex justify-content-center p-5" id="app-profile">
            <div class="card col-12 col-md-9 shadow-sm">
                <h5 class="card-title mb-5">
                    Mis datos
                </h5>
                <div class="card-body">
                    <?php if (session('message') !== null) : ?>
                        <div class="alert alert-message alert-light alert-primary alert-link mb-4">
                            <?= session('message') ?>
                        </div>
                    <?php endif ?>

                    <ValidationForm @submit="save" :validation-schema="vschema" v-slot="{ errors, dirty, valid, invalid,  isSubmitting }" ref="form-filters">
                        <?= csrf_field() ?>
                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-12">
                                <textinput v-model="datavm.first_name" type="text" id="first_name" name="first_name" label="Nombre"></textinput>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-12">
                                <textinput v-model="datavm.last_name" type="text" id="last_name" name="last_name" label="Apellido"></textinput>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-12">
                                <textinput v-model="datavm.business_name" type="text" id="business_name" name="business_name" label="Nombre comercial"></textinput>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-12">
                                <textinput v-model="datavm.username" type="text" id="username" name="username" label="Nombre de usuario" disabled="disabled"></textinput>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                                <textinput v-model="datavm.email" type="text" id="email" name="email" label="Email"></textinput>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                                <textinput id="phone_number" name="phone_number" v-model="datavm.phone_number" label="Teléfono"></textinput>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                                <genericselect id="shipping_id" name="shipping_id" v-model="datavm.shipping_id" label="Expreso de envío" :url-path="`api/shipping/select`"></genericselect>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                                <genericselect id="province_id" name="province_id" v-model="datavm.province_id" label="Provincia" url-path="api/province/select"></genericselect>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                                <genericselect id="city_id" name="city_id" v-model="datavm.city_id" label="Ciudad" :url-path="`api/province/cities/${datavm.province_id}`"></genericselect>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                                <textinput id="address" name="address" v-model="datavm.address" label="Dirección"></textinput>
                            </div>
                        </div>
                        <fieldset class="mb-4">
                            <legend>Datos de facturación</legend>
                            <div class="row">
                                <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                                    <genericselect id="billing_province_id" name="billing_province_id" v-model="datavm.billing_province_id" label="Provincia" url-path="api/province/select"></genericselect>
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                                    <genericselect id="billing_city_id" name="billing_city_id" v-model="datavm.billing_city_id" label="Ciudad" :url-path="`api/province/cities/${datavm.billing_province_id}`"></genericselect>
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                                    <textinput id="billing_address" name="billing_address" v-model="datavm.billing_address" label="Dirección"></textinput>
                                </div>
                            </div>

                        </fieldset>
                        <div class="d-grid col-12 col-md-8 mx-auto m-3">
                            <button class="btn btn-primary btn-block" :disabled="isSubmitting">
                                <span v-show="isSubmitting" class="spinner-border spinner-border-sm mr-1"></span>
                                Guardar cambios
                            </button>
                        </div>
                    </ValidationForm>
                </div>
            </div>
        </div>
    </div>
</main>
<!-- End of Main Content -->

<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script type="text/javascript" src="<?= base_url("dist/js/catalogue/profile.js"); ?>"></script>
<?= $this->endSection('scripts') ?>