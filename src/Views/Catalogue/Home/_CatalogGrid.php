  <!-- Catalogue mode Grid -->
  <div class="row cols-2 cols-sm-3 product-wrapper" v-if="modeView == 'grid'">
      <productcatalogue v-for="(product, index) in products" :dataProduct="product" :currency="filters.currency">

      </productcatalogue>
  </div>
  <!-- End Catalogue mode Grid -->