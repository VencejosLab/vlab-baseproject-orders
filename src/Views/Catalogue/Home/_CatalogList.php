  <!-- Start Catalogue mode List -->
  <div :class="modeView == 'list' ? 'product-lists product-wrapper' :  'row cols-2 cols-sm-3 product-wrapper'">
      <productcatalogue v-for="(product, index) in products" :dataProduct="product" :currency="filters.currency" :modeView="modeView">
      </productcatalogue>
  </div>
  <!-- End Catalogue mode List -->