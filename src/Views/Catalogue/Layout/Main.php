<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

    <title><?= env("NAME_COMPANY"); ?></title>

    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Comprabox - Ultimate eCommerce Template">
    <meta name="author" content="D-THEMES">

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="<?= base_url('dist/comprabox/images/icons/favicon.png'); ?>">

    <script>
        WebFontConfig = {
            google: {
                families: ['Poppins:400,500,600,700,800']
            }
        };
        (function(d) {
            var wf = d.createElement('script'),
                s = d.scripts[0];
            wf.src = 'dist/comprabox/js/webfont.js';
            wf.async = true;
            s.parentNode.insertBefore(wf, s);
        })(document);
    </script>
    <script>
        var APP = APP || {};
        (function(APP) {
            APP.BASEURL = '<?= base_url(); ?>';
        })(APP);
    </script>

    <style>
        .float {
            position: fixed;
            width: 80px;
            height: 80px;
            bottom: 100px;
            right: 30px;
            background-color: #25d366;
            color: #FFF;
            border-radius: 50px;
            text-align: center;
            font-size: 50px;
            box-shadow: 2px 2px 3px #999;
            z-index: 100;
        }

        .my-float {
            margin-top: 16px;
        }

        #loading {
            display: none;
            position: fixed;
            z-index: 999999;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            opacity: 0.5;
            background: #000 url('<?php echo base_url("dist/images/loading.gif"); ?>') no-repeat;
            background-position: center center;
            background-size: 5%;
        }

        /* When the body has the loading class, we turn
               the scrollbar off with overflow:hidden */
        body.is_loading {
            overflow: hidden;
        }

        /* Anytime the body has the loading class, our
               modal element will be visible */
        body.is_loading #loading {
            display: block;
        }
    </style>

    <link rel="stylesheet" type="text/css" href="<?= base_url('dist/comprabox/vendor/fontawesome-free/css/all.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('dist/comprabox/vendor/animate/animate.min.css'); ?>">

    <!-- Plugins CSS File -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('dist/comprabox/vendor/magnific-popup/magnific-popup.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('dist/comprabox/vendor/owl-carousel/owl.carousel.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('dist/comprabox/vendor/nouislider/nouislider.min.css'); ?>">

    <link rel="stylesheet" type="text/css" href="<?= base_url('dist/comprabox/vendor/sticky-icon/stickyicon.css'); ?>">

    <!-- Main CSS File -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('dist/comprabox/css/demo4.min.css'); ?>">

    <!-- Main CSS File -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('dist/comprabox/css/style.min.css'); ?>">

    <!-- Common CSS File -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('dist/css/common/common.css'); ?>">
</head>

<body>
    <div id="loading"></div>
    <div class="page-wrapper" id="app">
        <?= $this->include('/Landing/Comprabox/Layout/_Header') ?>
        <?= $this->renderSection('content') ?>
        <!-- End of Main -->
        <?= $this->include('/Landing/Comprabox/Layout/_Footer') ?>
        <!-- End Footer -->
    </div>

    <!-- Sticky Footer -->
    <?= $this->include('/Landing/Comprabox/Layout/_StickyFooter') ?>

    <!-- Scroll Top -->
    <a id="scroll-top" href="#top" title="Top" role="button" class="scroll-top"><i class="d-icon-arrow-up"></i></a>

    <!-- MobileMenu -->
    <?= $this->include('/Landing/Comprabox/Layout/_MobileMenu') ?>

    <!-- Plugins JS File -->
    <script src="<?= base_url('dist/comprabox/vendor/jquery/jquery.min.js'); ?>"></script>
    <script src="<?= base_url('dist/comprabox/vendor/sticky/sticky.min.js'); ?>"></script>
    <script src="<?= base_url('dist/comprabox/vendor/imagesloaded/imagesloaded.pkgd.min.js'); ?>"></script>
    <script src="<?= base_url('dist/comprabox/vendor/elevatezoom/jquery.elevatezoom.min.js'); ?>"></script>
    <script src="<?= base_url('dist/comprabox/vendor/magnific-popup/jquery.magnific-popup.min.js'); ?>"></script>
    <script src="<?= base_url('dist/comprabox/vendor/owl-carousel/owl.carousel.min.js'); ?>"></script>
    <script src="<?= base_url('dist/comprabox/vendor/nouislider/nouislider.min.js'); ?>"></script>
    <script src="<?= base_url('dist/js/util/shoppingcart.js'); ?>"></script>
    <!-- Main JS File -->
    <script src="<?= base_url('dist/comprabox/js/main.js'); ?>"></script>
    <?= $this->renderSection('scripts') ?>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <a href="https://api.whatsapp.com/send?phone=51955081075&text=Hola%21%20Quisiera%20m%C3%A1s%20informaci%C3%B3n%20sobre%20Varela%202." class="float" target="_blank">
        <i class="fa fa-whatsapp my-float"></i>
    </a>
</body>