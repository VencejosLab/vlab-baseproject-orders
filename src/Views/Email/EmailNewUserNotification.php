<?= $this->extend('Email/Layout/Main') ?>
<?= $this->section('content') ?>
<h2>Bienvenido a <?= env('TITLE_WEBSITE'); ?></h2>
<p>
    Hemos creado un nuevo usuario en nuestra plataforma para usted. Puede acceder con las siguientes credenciales en <a href="<?= site_url(); ?>"><?= site_url(); ?></a>
</p>
<div style="text-align: center">
    <p><b>Usuario:</b> <?= $userName ?></p>
    <p><b>Contraseña (temporal):</b> <?= $tmpPass; ?></p>
</div>
<table role="presentation" border="0" cellpadding="0" cellspacing="0" style="width: 100%;" width="100%">
    <tbody>
        <tr>
            <td style="line-height: 20px; font-size: 20px; width: 100%; height: 20px; margin: 0;" align="left" width="100%" height="20">
                &#160;
            </td>
        </tr>
    </tbody>
</table>
<p><?= lang('Auth.emailDate') ?> <?= esc($date) ?><br>
    Email System Generated for a New User.</p>
<?= $this->endSection() ?>