<?= $this->extend('Email/Layout/Main') ?>
<?= $this->section('content') ?>
<div>
    <table style="width: 100%; padding: 0px">
        <tr>
            <td style="text-align: center; width: 100%">
                <h3>¡Hemos confirmado su orden de pedido!</h3>
            </td>
        </tr>
        <tr>
            <td style="text-align: center; width: 100%">
                <p> Hola, <?= $order->client_fullname ?> </p> <br>
                <p> Datos de su pedido </p>
                <p>
                    <ul style="list-style: none;">
                        <li><b>Número: </b> <?= $order->number ?></li>
                        <li><b>Items: </b> <?= $order->items ?></li>
                        <li><b>Total: </b> <?= $order->total_net ?></li>
                    </ul>
                </p>
            </td>
        </tr>
        <tr>
            <td style="text-align: center; width: 100%">
                <p> Ante cualquier consulta puede ponerse en contacto mediante nuestras vías de comunicación. </p>
                <p> Muchas gracias por confiar en nosotros.</p>
            </td>
        </tr>
    </table>
</div>
<?= $this->endSection() ?>