<!DOCTYPE html>
<html>

<head>
    <title></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        :root {
            --main-color: #E94E1B;
            --secondary-color: #666;
            --text-color: #666
        }

        html {
            font-family: sans-serif;
            font-size: 14px;
        }

        body {
            margin: 0;
            padding: 0;
            min-width: 100% !important;
        }

        table,
        tbody,
        tr {
            display: block;
        }

        #footer-img {
            background-image: url('/dist/images/logo.jpg');
            height: 100px;
            width: 100%;
            min-height: 100px;
            margin-bottom: 0px !important;
        }

        #redes-sociales,
        #copy {
            margin: 0 auto;
            text-align: center;
        }

        #copy {
            font-size: 12px;
            text-align: center;
            margin: 0 auto;
        }

        .footer {
            width: 100%;
            padding: 0px;
            background-color: #E94E1B;
        }

        .footer>td>ul {
            list-style: none;
        }

        .footer>td>ul>li>a {
            color: #fff;
            text-decoration: none;
        }

        .main {
            background: #eee;
            width: 100%;
            padding: 10px;
        }

        .content {
            width: 100%;
            max-width: 800px
        }
    </style>
</head>

<body>
    <table class="content" border="0" align="center">
        <tr>
            <td>
                <div>
                    <header>
                        <div id="logo-head">
                            <a href="<?= base_url(); ?>" title="<?= env('NAME_COMPANY') ?>">
                                <img src="<?= base_url('dist/images/logo.jpg'); ?>" alt="<?= env('NAME_COMPANY') ?>" width="200" />
                            </a>
                        </div>
                    </header>
                </div>
            </td>
        </tr>
        <tr class="main" style="border-top: 1px solid #ccc;">
            <td>
                <div>
                    <section>
                        <?= $this->renderSection('content') ?>
                    </section>
                </div>
            </td>
        </tr>
        <tr class="footer">
            <td>
                <ul>
                    <li>
                        <h4 style="color:#fff; font-weight: bold; font-size: 15px;">Redes Sociales</h4>
                    </li>
                    <li>
                        <a href="#"> Facebook</a>
                    </li>
                    <li>
                        <a href="#"> Instagram</a>
                    </li>
                    <li>
                        <a href="#"> Twitter</a>
                    </li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>
                        <h4 style="color:#fff; font-weight: bold; font-size: 15px;">Contacto</h4>
                    </li>
                    <li>
                        <a href="#">Email: email@comprabox.com</a>
                    </li>
                    <li>
                        <a href="#">Tel: XXXX-XXXXXXX</a>
                    </li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>
                        <h4 style="color:#fff; font-weight: bold; font-size: 15px;">Informaci&oacute;n</h4>
                    </li>
                    <li>
                        <a href="#">Enlace 1</a>
                    </li>
                    <li>
                        <a href="#">Enlace 2</a>
                    </li>
                    <li>
                        <a href="#">T&eacute;rminos y condiciones</a>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
    <div style="margin:auto;max-width:48em" id="copy">
        <p> <?= env('NAME_COMPANY') . " " . date('Y') ?> - &COPY; Todos los derechos reservados.</p>
    </div>
</body>

</html>