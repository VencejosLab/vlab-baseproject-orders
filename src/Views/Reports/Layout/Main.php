<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <title></title>

    <style>
        #top-company-data {
            font-family: 'Poppins,sans-serif';
            font-size: 16px;
        }

        #report-title {
            font-family: 'Poppins,sans-serif' !important;
            font-size: 20px;
            font-weight: bold;
        }

        #report-footer {
            font-family: 'Poppins,sans-serif';
        }

        #timestamp{
            font-family: 'Poppins,sans-serif';
            font-size: 10px !important;
        }
    </style>
    <?= $this->renderSection('styles') ?>
</head>

<body>
    <!-- Header report -->
    <?= $this->include('/Reports/Layout/_Header') ?>
    <!-- Content -->
    <?= $this->renderSection('content') ?>
    <!-- Footer report -->
    <?= $this->include('/Reports/Layout/_Footer') ?>
</body>

</html>