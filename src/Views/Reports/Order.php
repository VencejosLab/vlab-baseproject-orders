<?= $this->extend('Reports/Layout/Main') ?>
<?= $this->section('styles') ?>
<style>
    #order-info {
        font-family: 'Poppins,sans-serif';
        font-size: 14px;
        float: right;
    }

    #table-details thead {
        background-color: #e2e2e2;
        font-family: 'Poppins,sans-serif' !important;
        font-size: 14px;
        font-weight: 600;
        text-align: center;
    }

    #table-details tbody {
        font-family: 'Poppins,sans-serif';
        font-size: 12px;
        font-weight: normal;
    }
</style>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="mb-3" id="report-header">
    <table class="table table-borderless">
        <tr>
            <td style="width: 40%; padding:0%;">
                <div class="text-center" id="report-title">
                    Nota de pedido <br>
                    <?= $data['number']; ?>
                </div>
            </td>
            <td style="width: 60%; padding:0%;">
                <div id="order-info">
                    <b>Fecha de pedido: </b><?= $data['date']; ?><br>
                    <b>Cliente:</b> <?= $data['client_fullname']; ?><br>
                    <b>Estado:</b> <?= $data['status_name']; ?></br>
                </div>
            </td>
        </tr>
    </table>
</div>
<br>
<!-- Details order -->
<table id="table-details" class="table table-bordered table-sm" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th scope="col">N°</th>
            <th scope="col">Producto</th>
            <th scope="col">Cantidad</th>
            <th scope="col">Precio</th>
            <th scope="col">SubTotal</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data['details'] as $key => $detail) { ?>
            <tr>
                <td style="width: 10%" class="text-center"><?= $key + 1; ?></td>
                <td style="width: 30%">
                    <?= $detail['product_name']; ?><br>
                    <small><?= $detail['product_code']; ?></small>
                </td>
                <td style="width: 30%">
                    <?php if ($detail['bulk_items'] > 1) { ?>
                        <?= $detail['quantity_format'] . " bulto(s)" ?><br>
                        Un. por bulto: <b> <?= $detail['bulk_items_format'] ?></b><br>
                        P. unidad: <b><?= $detail['bulk_unit_price_format'] ?></b>
                    <?php } else { ?>
                        <?= $detail['quantity_format'] . " unidad(es)" ?>
                    <?php } ?>
                </td>
                <td style="width: 15%" class="text-right"><?= $detail['unit_price_format'] ?></td>
                <td style="width: 15%" class="text-right"><?= $detail['subtotal_format'] ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<!-- Total order -->
<table class="table table-borderless">
    <tr>
        <td style="width: 100%; padding:0%; text-align: right;">
            <p> TOTAL: <b><?= $data['total_net'] ?></b></p>
        </td>
    </tr>
</table>
<!-- End of Main Content -->
<?= $this->endSection() ?>