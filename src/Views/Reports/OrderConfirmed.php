<?= $this->extend('Reports/Layout/Main') ?>
<?= $this->section('styles') ?>
<style>
    #order-info {
        font-family: 'Arial, Helvetica, sans-serif';
        font-size: 14px;
        float: right;
    }

    #table-details thead {
        background-color: #e2e2e2;
        font-family: 'Arial, Helvetica, sans-serif' !important;
        font-size: 14px;
        font-weight: 600;
        text-align: center;
    }

    #table-details tbody {
        font-family: 'Arial, Helvetica, sans-serif';
        font-size: 12px;
        font-weight: normal;
    }
</style>
<?= $this->endSection() ?>
<?= $this->section('content') ?>
<div class="mb-3" id="report-header">
    <table class="table table-borderless">
        <tr>
            <td style="width: 40%; padding:0%;">
                <div class="text-center" id="report-title">
                    Nota de pedido <br>
                    <?= $data['number']; ?>
                </div>
            </td>
            <td style="width: 60%; padding:0%;">
                <div id="order-info">
                    <b>Fecha de pedido: </b><?= $data['date']; ?><br>
                    <b>Cliente:</b> <?= $data['client_fullname']; ?><br>
                    <b>Estado:</b> <?= $data['status_name']; ?></br>
                    <b>Entrega:</b> <?= $data['shipping_method_name']; ?></br>
                    <!-- Fecha de envío -->
                    <?php if ($data['shipping_method'] == 'delivery' && $data['shipping_date'] !== "00/00/0000 00:00") { ?>
                        <b>Fecha de envío:</b> <?= $data['shipping_date']; ?></br>
                    <?php } ?>
                    <!-- Fecha de entrega -->
                    <?php if ($data['delivery_date'] !== "00/00/0000 00:00") { ?>
                        <b>Fecha de entrega:</b> <?= $data['delivery_date']; ?></br>
                    <?php } ?>
                </div>
            </td>
        </tr>
    </table>
</div>
<br>
<table id="table-details" class="table table-bordered table-sm" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th scope="col">N°</th>
            <th scope="col">Producto</th>
            <th scope="col">Cant. solicitada</th>
            <th scope="col">Precio</th>
            <th scope="col">Subtotal</th>
            <th scope="col">Cant. confirmada</th>
            <th scope="col">Subtotal confirmado</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data['details'] as $key => $detail) { ?>
            <tr>
                <td style="width: 5%" class="text-center">
                    <?= $key + 1; ?>
                </td>
                <td style="width: 30%">
                    <?= $detail['product_name']; ?><br>
                    <small><?= $detail['product_code']; ?></small>
                </td>
                <td style="width: 20%" class="text-center">
                    <?php if ($detail['bulk_items'] > 1) { ?>
                        <?= $detail['quantity_format'] . " bulto(s)" ?><br>
                        Un. por bulto: <b> <?= $detail['bulk_items_format'] ?></b><br>
                        P. unidad: <b><?= $detail['bulk_unit_price_format'] ?></b>
                    <?php } else { ?>
                        <?= $detail['quantity_format'] . " unidad(es)" ?>
                    <?php } ?>
                </td>
                <td style="width: 10%" class="text-right">
                    <?= $detail['unit_price_format'] ?>
                </td>
                <td style="width: 15%" class="text-right">
                    <?= $detail['subtotal_format'] ?>
                </td>
                <td style="width: 20%" class="text-center">
                    <?php if ($detail['bulk_items'] > 1) { ?>
                        <?= $detail['confirmed_quantity_format'] . " bulto(s)" ?><br>
                    <?php } else { ?>
                        <?= $detail['confirmed_quantity_format'] . " unidad(es)" ?>
                    <?php } ?>
                </td>
                <td style="width: 15%" class="text-right">
                    <?= $detail['confirmed_subtotal_format'] ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<!-- Total order -->
<table class="table table-borderless">
    <tr>
        <td style="width: 100%; padding:0%; text-align: right;">
            <p> TOTAL: <b><?= $data['total_net'] ?></b></p>
            <p> TOTAL CONFIRMADO:<b> <?= $data['total_confirmed'] ?></b></p>
        </td>
    </tr>
</table>
<!-- End of Main Content -->
<?= $this->endSection() ?>