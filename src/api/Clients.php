<?php

namespace VLab\BaseOrders\Api;

use CodeIgniter\HTTP\RequestInterface;
use Psr\Log\LoggerInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use Config\Services;
use VLab\BaseOrders\Entities\ClientEntity;
use VLab\BaseOrders\Entities\UserEntity;
use VLab\BaseOrders\Models\ClientsModel;
use VLab\BaseOrders\Models\UsersModel;

class Clients extends ResourceController
{
    use ResponseTrait;

    protected $request;
    protected $auth;
    protected $user;
    protected $permissions;


    protected $validation;


    /**
     * @var ClientsModel
     */
    protected $model;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        $this->response = Services::response();
        $this->request = \Config\Services::request();
        $this->model = new ClientsModel();
        $this->validation = \Config\Services::validation();

        $this->validation->setRules([
            'first_name'                => ['label' => 'Nombre',            'rules'          => 'required'],
            'last_name'                 => ['label' => 'Apellido',          'rules'          => 'required'],
            'business_name'             => ['label' => 'Nombre comercial',  'rules'          => 'required'],
            'email'                     => ['label' => 'Email',             'rules'          => 'required|valid_email'],
            'user_email'                => ['label' => 'Email de acceso',   'rules'          => 'required|valid_email'],
            'user_username'             => ['label' => 'Nombre de usuario', 'rules'          => 'required|max_length[30]|min_length[3]']
        ]);
    }

    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    public function index()
    {
        try {
            $data = [];
            if ($this->request->isAJAX()) {
                $requestData = $this->request->getGet();
                $data = $this->model->search($requestData['username'] ?? '', $requestData['business_name'] ?? '', $requestData['email'] ?? '', $requestData['contact_person'] ?? '', $requestData['OrderBy'], $requestData['PageIndex'], $requestData['PageSize']);
            }

            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Return the properties of a resource object
     *
     * @return mixed
     */
    public function show($id = null)
    {
        try {
            if ($id == 0) {
                $data = new ClientEntity();
            } else {
                $data = $this->model->find($id);
            }
            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Return a new resource object, with default properties
     *
     * @return mixed
     */
    public function new()
    {
        try {
            $data = new ClientEntity();
            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Create a new resource object, from "posted" parameters
     *
     * @return mixed
     */
    public function create()
    {
        try {
            $data = (array)$this->request->getJSON();

            if ($this->validation->run($data) == FALSE) {
                $response['status']    = false;
                $response['messages']   = $this->validation->getErrors(); //Show Error in Input Form

                return $this->respond($response, 422);
            }

            $entity = new ClientEntity($data);
            $result = $this->model->insert($entity, true);
            $response = [
                'status' => true,
                'data' => ['id' => $result]
            ];
            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Return the editable properties of a resource object
     *
     * @return mixed
     */
    public function edit($id = null)
    {
        try {
            $data = $this->model->find($id);
            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Add or update a model resource, from "posted" properties
     *
     * @return mixed
     */
    public function update($id = null)
    {
        try {
            $data = (array)$this->request->getJSON();

            if ($this->validation->run($data) == FALSE) {
                $response['status']    = false;
                $response['messages']   = $this->validation->getErrors(); //Show Error in Input Form

                return $this->respond($response, 422);
            }

            $entity = new ClientEntity();
            $entity->fill($data);
            $entity->has_required_data = 1;
            $result = $this->model->update($id, $entity);

            //edit user data
            $usersModel = new UsersModel();
            $entityUser = new UserEntity();
            $entityUser->id = $data['user_id'];
            $entityUser->first_name = $data['first_name'];
            $entityUser->last_name = $data['last_name'];

            $result = $usersModel->update($data['user_id'], $entityUser);

            $response = [
                'status' => true,
                'data' => ['id' => $id]
            ];
            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Delete the designated resource object from the model
     *
     * @return mixed
     */
    public function delete($id = null)
    {
        try {
            $original = $this->model->find($id);
            $result = $this->model->delete($id);
            $response = [
                'status' => true,
                'data' => ['id' => $id]
            ];
            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Get list of resources to select list
     * 
     * @return array
     */
    public function select()
    {
        try {
            $data = $this->model->select('id, business_name AS text')->orderBy('business_name')->get()->getResult();

            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            return $this->respond([], 500);
        }
    }
}
