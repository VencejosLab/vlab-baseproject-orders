<?php

namespace VLab\BaseOrders\Api;

use CodeIgniter\HTTP\RequestInterface;
use Psr\Log\LoggerInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use Config\Services;
use VLab\BaseOrders\Entities\SettingEntity;
use VLab\BaseOrders\Models\CurrenciesModel;
use VLab\BaseOrders\Models\SettingsModel;

class Settings extends ResourceController
{
    use ResponseTrait;

    protected $request;
    protected $auth;
    protected $user;
    protected $permissions;


    protected $validation;


    /**
     * @var SettingsModel
     */
    protected $model;

    /**
     * @var CurrenciesModel
     */
    protected $modelCurrency;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        $this->response = Services::response();
        $this->request = \Config\Services::request();
        $this->model = new SettingsModel();
        $this->modelCurrency = new CurrenciesModel();
        $this->validation = \Config\Services::validation();

        $this->validation->setRules([]);
    }

    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    public function index()
    {
        try {
            $data = [];
            $requestData = $this->request->getGet();
            $data = $this->model->search($requestData['group'] ?? ""); //Call model query    
            $result = [];
            foreach ($data as $key => $element) {
                $result[$element['group']][$element['key']][] = [
                    'id'    => $element['id'],
                    'value' => $element['value'],
                    'type'  => $element['type']
                ];
            }

            return $this->respond($result, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }


    public function defaults()
    {
        try {
            $data = [];
            $requestData = $this->request->getGet();
            $data = $this->model->search($requestData['group'] ?? ""); //Call model query    
            $result = [];
            foreach ($data as $key => $element) {
                $result[$element['key']] = [
                    'value' => $element['value']
                ];

                if ($element['key'] == 'default_currency') {
                    $currency = $this->modelCurrency->where(['company_id' => session('company_id'), 'iso_code' => $element['value']])->first();
                    $result['default_currency_exchange_rate'] = [
                        'value' => $currency->exchange_rate
                    ];
                }

                if ($element['key'] == 'default_currency_admin') {
                    $currency = $this->modelCurrency->where(['company_id' => session('company_id'), 'iso_code' => $element['value']])->first();
                    $result['default_currency_admin_exchange_rate'] = [
                        'value' => $currency->exchange_rate
                    ];
                }
            }

            return $this->respond($result, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }


    /**
     * Return the properties of a resource object
     *
     * @return mixed
     */
    public function show($id = null)
    {
        try {
            if ($id == 0) {
                $data = new SettingEntity();
            } else {
                $data = $this->model->find($id);
            }
            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Return a new resource object, with default properties
     *
     * @return mixed
     */
    public function new()
    {
        try {
            $data = new SettingEntity();
            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Create a new resource object, from "posted" parameters
     *
     * @return mixed
     */
    public function create()
    {
        try {
            $dataPost = (array)$this->request->getJSON();
            $originals = $this->model->search(""); //Call model query   

            $groupSettings = $this->groupSettings($originals);

            $originalGenerals = $groupSettings['generals'];
            /**
             * $key => key de la setting
             */
            foreach ($dataPost['generals'] as $key => $setting) {
                for ($i = 0; $i < sizeof($setting); $i++) {
                    if ($setting[$i]->value != $originalGenerals[$key][$i]['value']) {
                        $dataUpdate = new SettingEntity();
                        $dataUpdate->id = $setting[$i]->id;
                        $dataUpdate->value = $setting[$i]->value;

                        $this->model->save($dataUpdate);
                    }
                }
            }

            $originalProducts = $groupSettings['products'];

            foreach ($dataPost['products'] as $key => $setting) {
                for ($i = 0; $i < sizeof($setting); $i++) {
                    if ($setting[$i]->value != $originalProducts[$key][$i]['value']) {
                        $dataUpdate = new SettingEntity();
                        $dataUpdate->id = $setting[$i]->id;
                        $dataUpdate->value = $setting[$i]->value;

                        $this->model->save($dataUpdate);
                    }
                }
            }

            $response = [
                'status' => true
            ];
            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Return the editable properties of a resource object
     *
     * @return mixed
     */
    public function edit($id = null)
    {
        //
    }

    /**
     * Add or update a model resource, from "posted" properties
     *
     * @return mixed
     */
    public function update($id = null)
    {
        try {
            $data = (array)$this->request->getJSON();



            $response = [
                'status' => true,
                'data' => ['id' => $id]
            ];
            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Delete the designated resource object from the model
     *
     * @return mixed
     */
    public function delete($id = null)
    {
        try {
            $original = $this->model->find($id);
            $result = $this->model->delete($id);
            $response = [
                'status' => true,
                'data' => ['id' => $id]
            ];
            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Get list of resources to select list
     * 
     * @return array
     */
    public function select()
    {
        try {
            $data = $this->model->select('id, name AS text')->orderBy('name')->get()->getResult();

            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            return $this->respond([], 500);
        }
    }

    /**
     * =============================== Helpers ================================
     */

    /**
     * Agrupa las settings por group y key y las retorna en un arreglo
     */
    private function groupSettings($settings)
    {
        $result = [];
        foreach ($settings as $key => $element) {
            $result[$element['group']][$element['key']][] = [
                'id'    => $element['id'],
                'value' => $element['value'],
                'type'  => $element['type']
            ];
        }

        return $result;
    }

    private function checkSettingsChanges($settingGroup)
    {
    }
}
