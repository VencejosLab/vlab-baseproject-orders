<?php

namespace VLab\BaseOrders\Api;

use CodeIgniter\HTTP\RequestInterface;
use Psr\Log\LoggerInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use Config\Services;
use CodeIgniter\HTTP\Exceptions\HTTPException;
use VLab\BaseOrders\Entities\Filters\ProductsFilters;
use VLab\BaseOrders\Entities\ProductEntity;
use VLab\BaseOrders\Entities\UploadEntity;
use VLab\BaseOrders\Models\ProductsModel;
use VLab\BaseOrders\Models\UploadsModel;

class Products extends ResourceController
{
    use ResponseTrait;

    protected $request;
    protected $auth;
    protected $user;
    protected $permissions;


    protected $validation;


    /**
     * @var ProductsModel
     */
    protected $model;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        $this->response = Services::response();
        $this->request = \Config\Services::request();
        $this->model = new ProductsModel();
        $this->validation = \Config\Services::validation();

        $this->validation->setRules([
            'code'              => ['label' => 'Código',            'rules'     => 'required|max_length[100]'],
            'name'              => ['label' => 'Nombre',            'rules'     => 'required|max_length[250]'],
            'sale_price'        => ['label' => 'Precio de venta',   'rules'     => 'required'],
            'stock'             => ['label' => 'Stock',             'rules'     => 'required'],
        ]);
    }

    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    public function index()
    {
        try {
            $data = [];
            if ($this->request->isAJAX()) {
                $requestData = $this->request->getGet();
                $filter = new ProductsFilters($requestData);
                $data = $this->model->search($filter->brand_id, $filter->category_id, $filter->supplier_id, $filter->name, $filter->code, $filter->OrderBy, $filter->PageIndex, $filter->PageSize);
            }

            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Return the properties of a resource object
     *
     * @return mixed
     */
    public function show($id = null)
    {
        try {
            if ($id == 0) {
                $data = new ProductEntity();
            } else {
                $data = $this->model->find($id);
            }
            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Return a new resource object, with default properties
     *
     * @return mixed
     */
    public function new()
    {
        try {
            $data = new ProductEntity();
            $data->active = 1;
            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Create a new resource object, from "posted" parameters
     *
     * @return mixed
     */
    public function create()
    {
        try {
            $data = (array)$this->request->getJSON();

            if ($this->validation->run($data) == FALSE) {
                $response['status']    = false;
                $response['messages']   = $this->validation->getErrors(); //Show Error in Input Form

                return $this->respond($response, 422);
            }

            $entity = new ProductEntity($data);
            $result = $this->model->insert($entity, true);
            $response = [
                'status' => true,
                'data' => ['id' => $result]
            ];
            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Return the editable properties of a resource object
     *
     * @return mixed
     */
    public function edit($id = null)
    {
        //
    }

    /**
     * Add or update a model resource, from "posted" properties
     *
     * @return mixed
     */
    public function update($id = null)
    {
        try {
            $data = (array)$this->request->getJSON();

            if ($this->validation->run($data) == FALSE) {
                $response['status']    = false;
                $response['messages']   = $this->validation->getErrors(); //Show Error in Input Form

                return $this->respond($response, 422);
            }

            $entity = new ProductEntity();
            $entity->fill($data);
            $result = $this->model->update($id, $entity);
            $response = [
                'status' => true,
                'data' => ['id' => $id]
            ];
            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }


    public function activate()
    {
        try {
            $data = (array)$this->request->getJSON();
            $producto = $this->model->find($data['id']);
            $producto->active = $data['active'];
            $result = $this->model->update($data['id'], $producto);

            $response = [
                'status' => $result
            ];
            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    public function desactivate()
    {
        try {
            $data = (array)$this->request->getJSON();
            $entity = new ProductEntity();
            $entity->fill($data);

            $result = $this->model->update($data['id'], $entity);

            $response = [
                'status' => $result
            ];
            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Delete the designated resource object from the model
     *
     * @return mixed
     */
    public function delete($id = null)
    {
        try {
            $original = $this->model->find($id);
            $result = $this->model->delete($id);
            $response = [
                'status' => true,
                'data' => ['id' => $id]
            ];
            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Get list of resources to select list
     * 
     * @return array
     */
    public function select()
    {
        try {
            $data = $this->model->select('id, name AS text')->orderBy('name')->get()->getResult();

            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Get list of resources to select list
     * 
     * @return array
     */
    public function getfeatures($id)
    {
        try {
            $data = $this->model->getProductFeatures($id);

            return $this->respond(['data' => $data], 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    public function getphotos($id)
    {
        try {
            $uploadsModel = new UploadsModel();

            $photos = $uploadsModel->select('id, original_name, new_name, extension')->where(['type' => "products", 'id_type' => $id])->get()->getResult();

            return $this->respond(['data' => $photos], 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    public function uploadPhotos()
    {
        try {
            $uploadsModel = new UploadsModel();
            $valid_extensions = array('jpg', 'jpeg', 'png');
            $max_file_size = 5000000; //5MB
            $overwrite   =  (bool) $this->request->getPost('replace') ?? true;
            $results = [];
            $product_id = (int) $this->request->getPost('product_id');

            if ($imagefile = $this->request->getFiles()) {
                foreach ($imagefile['files'] as $img) {
                    $fileExt = pathinfo($img->getClientExtension()); // get file extension
                    // allow valid file formats
                    if (!in_array($fileExt['basename'], $valid_extensions)) {
                        $results[] = [
                            'file'      => $img->getClientName(),
                            'success'   => false,
                            'message'   => "Formato de archivo no permitido. Sólo se aceptan " . implode(",", $valid_extensions)
                        ];
                        continue;
                    }

                    //Check file size
                    if ($img->getSize() > $max_file_size) {
                        $results[] = [
                            'file'      => $img->getClientName(),
                            'success'   => false,
                            'message'   => "El archivo subido no debe superar los 5 MB."
                        ];
                        continue;
                    }

                    if ($img->isValid() && !$img->hasMoved()) {
                        $newName = $img->getRandomName();
                        $img->move(WRITEPATH . 'uploads', $newName, $overwrite);
                    } else {
                        $results[] = [
                            'file'      => $img->getClientName(),
                            'success'   => false,
                            'message'   => $img->getErrorString() . '(' . $img->getError() . ')'
                        ];
                        continue;
                    }

                    $upload = new UploadEntity([
                        'original_name'     => $img->getClientName(),
                        'new_name'          => $newName,
                        'extension'         => $img->getClientExtension(),
                        'path'              => WRITEPATH . 'uploads/' . $newName,
                        'type'              => "products",
                        'id_type'           => $product_id,
                    ]);

                    $saved = $uploadsModel->insert($upload);

                    $results[] = [
                        'file'      => $img->getClientName(),
                        'success'   => true,
                        'message'   => "Subido correctamente"
                    ];
                }
            }

            return $this->respond(['status' => true, 'data' => $results]);
        } catch (\Throwable $th) {
            log_message('error', $th->__toString());
            $this->respond([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    public function uploadPhoto()
    {
        try {
            $data = json_decode(file_get_contents("php://input"), true); // collect input parameters and convert into readable format
            //Configs
            $upload_path = FCPATH . 'public/uploads/'; // set upload folder path 
            $max_file_size = 5000000; //5MB
            $valid_extensions = array('jpg', 'jpge', 'png');

            $fileName  =  $_FILES['files']['name'];
            $tempPath  =  $_FILES['sendfile']['tmp_name'];
            $fileSize  =  $_FILES['sendfile']['size'];
            $replace   =  (bool) $this->request->getPost('replace') ?? true;

            if (empty($fileName)) {
                // echo json_encode(array("message" => "please select a file", "status" => false));
                $this->respond(['status' => false, 'message' => "Archivo no encontrado."], 400);
            } else {
                if (!is_dir($upload_path)) {
                    if (!mkdir($upload_path, 0777, true)) {
                        $this->respond(['status' => false, 'message' => "No se pudo crear el directorio para subir el archivo."], 400);
                    }
                }

                $fileExt = strtolower(pathinfo($fileName, PATHINFO_EXTENSION)); // get file extension
                // allow valid file formats
                if (in_array($fileExt, $valid_extensions)) {
                    //check file does not exist in the upload folder path
                    if (!file_exists($upload_path . $fileName)) {
                        // check file size '5MB'
                        if ($fileSize < $max_file_size) {
                            if (move_uploaded_file($tempPath, $upload_path . $fileName)) { // move file from system temporary path to the upload folder path
                                $this->respond(['status' => true, 'message' => "Archivo subido correctamente.", 'data' => $upload_path . $fileName], 200);
                                // echo json_encode(array("message" => "File Uploaded Successfully", "status" => true));
                            } else {
                                $this->respond(['status' => false, 'message' => "El archivo no se pudo subir."], 500);
                                // echo json_encode(array("message" => "File couldn't be uploaded", "status" => false));
                            }
                        } else {
                            $this->respond(['status' => false, 'message' => "El archivo subido no debe superar los 5 MB."], 400);
                            // echo json_encode(array("message" => "Sorry, your file is too large, please upload 5 MB size", "status" => false));
                        }
                    } else {
                        if ($replace) {
                            if ($fileSize < $max_file_size) {
                                if (move_uploaded_file($tempPath, $upload_path . $fileName)) { // move file from system temporary path to the upload folder path
                                    $this->respond(['status' => true, 'message' => "Archivo subido correctamente.", 'data' => $upload_path . $fileName], 200);
                                } else {
                                    $this->respond(['status' => false, 'message' => "El archivo no se pudo subir."], 500);
                                }
                            } else {
                                $this->respond(['status' => false, 'message' => "El archivo no debe superar los 5 MB."], 400);
                            }
                        }
                        $this->respond(['status' => true, 'message' => "El archivo ya existe en el directorio."], 200);
                    }
                } else {
                    $this->respond(['status' => false, 'message' => "Formato de archivo no permitido. Sólo se aceptan " . implode(",", $valid_extensions)], 400);
                    // echo json_encode(array("message" => "Sorry, only JPG, JPEG, PNG, GIF, PDF, DOCX &amp; TEXT files are allowed", "status" => false));
                }
            }
        } catch (\Throwable $th) {
            //Log error
            log_message('error', $th->__toString());
            $this->respond([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
}
